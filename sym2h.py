#!/usr/bin/env python3

# Copyright (C) 2023 David Hunter
#
# This file is part of the zsnes-asm2c project.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import argparse
import re


skiplist = []
symtab = []


def skiplist_add(line):
    line = line.rstrip('\r\n')
    global skiplist
    skiplist.append(line)


def convert(line):
    line = line.rstrip('\r\n')

    if re.search(r'[.]', line):
        return ''

    m = re.match(r'([0-9a-f]{8}) ([atT]) (\S+)', line)
    if not m:
        return ''
    (addr, cls, name) = m.groups()
    #print(addr, cls, name)
    addr = '0x' + addr

    if cls == 'T' and name[0] == '_':
        name = name[1:]

    if name in skiplist:
        return ''

    global symtab
    if name in symtab:
        return ''
    symtab.append(name)

    return f'#define {name:31s} {addr}'

    return line


def get_args():
    parser = argparse.ArgumentParser(description='sym2h')
    parser.add_argument('-i', type=str, help='Input file (.sym)')
    parser.add_argument('--skip', type=str, help='Skip list')
    parser.add_argument('-o', type=str, help='Output file')

    return parser.parse_args()


def main():
    args = get_args()

    global skiplist
    with open(args.skip, 'r') as fin:
        for line in fin:
            skiplist_add(line)

    with open(args.i, 'r') as fin:
        with open(args.o, 'w') as fout:
            for line in fin:
                hline = convert(line)
                if hline:
                    print(hline, file=fout)


if __name__ == '__main__':
    main()
