#!/usr/bin/env python3

# Copyright (C) 2023 David Hunter
#
# This file is part of the zsnes-asm2c project.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import sys
import argparse
import re
from collections import namedtuple

#symtab = None

class AsmFile:
    def __init__(self, f, fn, out):
        self.f = f
        self.fn = fn
        self.out = out
        self.macro_def = ''
        self.macros = dict()
        self.mpid = 0

    def macro_open(self, args, ln):
        #print(args)
        args = args.split()
        args = list(map(str.strip, args))
        name = args[0]
        macro = namedtuple('macro', ['argc', 'line0', 'body'])
        macro.argc = int(args[1])
        macro.line0 = ln
        macro.body = []
        self.macro_def = name
        self.macros[self.macro_def] = macro

    def macro_add(self, line):
        if '%endm' in line:
            self.macro_def = ''
        else:
            self.macros[self.macro_def].body.append(line)

    def macro_find(self, line):
        for name in self.macros:
            m = re.match(f'\\s+{name}\\b', line)
            if m:
                return name
        return ''

    def macro_play(self, name, invoke_ln, line):
        macro = self.macros[name]
        argc = macro.argc
        ln0 = macro.line0

        m = re.match(f'\\s+{name}\\b\\s*(.*)', line)
        args = m.group(1).strip()
        args = args.split(',') if args else []
        args = list(map(str.strip, args))
        assert len(args) == argc

        self.mpid += 1
        mpid = self.mpid
        for line in macro.body:
            ln = ln0 + 1
            try:
                for i in range(argc):
                    line = re.sub(f'%{i+1}', args[i], line)
                line = re.sub('%%', f'_{mpid}_', line)
                self.convert_line(ln, line)
                ln += 1
            except Exception as e:
                print(f'{self.fn}:{ln}: invoked by macro {name}')
                raise e

    def convert_oper(self, o):
        o = o.strip()
        o = re.sub(r'\b(near|far|short)\b\s*', '', o)
        o = re.sub(r'\b(cs|ds|es):', '', o)
        o = re.sub(r'\b(\d[0-9A-Fa-f]*)h\b', '0x\\1', o)
        o = re.sub(r'\b([01]+)b\b', '0b\\1', o)
        o = re.sub(r'\b0(\d+)\b', '\\1', o)

        m = re.search(r'(\S*)\s*\[([^\]]+)\]', o)
        if m:
            (width, addr) = m.groups()
            addr = re.sub(r'(byte|word|dword)\s*', '', addr)
            if not width:
                width = '*'
            o = f'm_{width}({addr})'

        o = re.sub(r'\.', '_', o)

        return o

    def reg_width(self, reg):
        if re.fullmatch(r'e([abcd]x|[sd]i|bp)', reg):
            return 'dword'
        elif re.fullmatch(r'([abcd]x|[sd]i|bp)', reg):
            return 'word'
        elif re.fullmatch(r'([abcd][lh])', reg):
            return 'byte'
        assert False

    def implied_width(self, opers):
        mi = 0 if 'm_*' in opers[0] else 1
        ri = 1 - mi
        width = self.reg_width(opers[ri])
        return (width, mi)

    def convert_ins(self, ins):
        m = re.match(r'(\S+)\s*(.*)', ins)
        if not m:
            return ('', None, [])
        (opc, opers) = m.groups()
        opers = list(map(self.convert_oper, opers.split(',')))
        if opers == ['']:
            opers = []
        opcw = None
        if len(opers) == 2 and 'm_*' in ''.join(opers):
            (width, mi) = self.implied_width(opers)
            opers[mi] = re.sub(r'm_\*', f'm_{width}', opers[mi])
        if len(opers) and opc in ['imul', 'mul', 'div']:
            opcw = self.reg_width(opers[0])
        return (opc, opcw, opers)

    def convert_line(self, ln, line):
        out = self.out

        if ';;; asm2c:' in line:
            if ':ENDSYM' in line:
                out.end()
            elif ':CLINE' in line:
                m = re.search(r':CLINE (.*)', line)
                out.cline(m.group(1))
            elif ':EXTERN' in line:
                m = re.search(r':EXTERN \S+ (.*)', line)
                out.extern(m.group(1))
            return
        elif ';' in line:
            (line, comment) = re.match(r'(.*?);(.*)', line).groups()
            out.push_comment(comment)
        line = line.rstrip()

        if re.match(r'%macro', line):
            self.macro_open(line[7:].strip(), ln)
            return
        macro = self.macro_find(line)
        if macro:
            self.macro_play(macro, ln, line)
            return

        (label, ins) = ('', '')
        m = re.match(r'(\S*)\s*(.*)', line)
        if m:
            (label, ins) = m.groups()
            if label and label[-1] == ':':
                label = label[:-1]
        if label == 'NEWSYM' and ins:
            out.open_fn(ins)
        else:
            if label:
                label = re.sub(r'\.', '_', label)
                out.label(label)
            (opc, opcw, opers) = self.convert_ins(ins)
            out.ins(opc, opcw, *opers)

    def input_line(self, ln, line):
        line = line.rstrip('\r\n')
        if self.macro_def:
            self.macro_add(line)
        else:
            self.convert_line(ln, line)


class Outfile:
    def __init__(self, f):
        self.f = f
        self.indent = 0

    def print(self, s=''):
        s = '\t' * self.indent + s
        print(s, file=self.f)


class Cfile(Outfile):
    def __init__(self, f):
        Outfile.__init__(self, f)
        self.comment = None

        self.print('#include "asm2c.h"')
        self.print()

    def print(self, s=''):
        if self.comment is not None:
            if s:
                s = s + '\t'
            s = s + '//' + self.comment
            self.comment = None
        Outfile.print(self, s)

    def push_comment(self, s):
        self.comment = s

    def open_fn(self, fname):
        self.print(f'void {fname}(void)')
        self.print('{')
        self.indent = 1

    def close_fn(self):
        self.indent = 0
        self.print('}')
        self.print()

    def end(self):
        if self.indent:
            self.close_fn()

    def label(self, lb):
        oi = self.indent
        self.indent = self.indent - 1
        self.print(f'{lb}:')
        self.indent = oi

    def ins(self, opc, opcw, *opers):
        c = ''
        if opc:
            sopers = ', '.join(opers)
            if opcw:
                opc = opc + '_' + opcw
            opcc = len(opers)
            c = f'op{opcc}_{opc}({sopers});'
        self.print(c)

    def cline(self, line):
        self.print(line)

    def extern(self, decl):
        self.print('extern ' + decl + ';')


class Hfile(Outfile):
    def __init__(self, f):
        Outfile.__init__(self, f)

        self.print('#pragma once')
        self.print()

    def push_comment(self, s):
        pass

    def open_fn(self, fname):
        self.print(f'void {fname}(void);')

    def close_fn(self):
        pass

    def end(self):
        pass

    def label(self, lb):
        pass

    def ins(self, opc, width, *opers):
        pass

    def cline(self, line):
        pass

    def extern(self, decl):
        pass


def get_args():
    parser = argparse.ArgumentParser(description='asm2c')
    #parser.add_argument('--sym', type=str, help='Path to .sym')
    parser.add_argument('-i', type=str, help='Input file (.asm)')
    parser.add_argument('-o', type=str, help='Output file (.c, .h)')
    parser.add_argument('-t', type=str, help='Output type (c, h)')

    return parser.parse_args()


def main():
    args = get_args()
    #global symtab
    #symtab = read_sym(args.sym)

    with open(args.i, 'r') as fin:
        fn = args.i
        with open(args.o, 'w') as fout:
            if args.t == 'c':
                out = Cfile(fout)
            elif args.t == 'h':
                out = Hfile(fout)
            ain = AsmFile(fin, fn, out)
            ln = 1
            for line in fin:
                if line == "\x1A": # ^Z: DOS EOF
                    break
                try:
                    ain.input_line(ln, line)
                except Exception as e:
                    print(f'{fn}:{ln}: {repr(e)}: {e}')
                    raise e
                ln += 1


if __name__ == '__main__':
    main()
