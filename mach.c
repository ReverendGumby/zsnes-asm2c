/*
 * Copyright (C) 2023 David Hunter
 *
 * This file is part of the zsnes-asm2c project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "asm2c.h"
#include "mach.h"

#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>

mach_init_t init;
uint8_t* _ram;
state_t _st;
size_t _ram_size = 0x00140000;

void mach_init(const mach_init_t* in_init)
{
    init = *in_init;
    _ram = malloc(_ram_size);
}

void mach_init_state(void)
{
    eax = 0;
    ebx = 0;
    ecx = 0;
    edx = 0;
    esp = _ram_size;
    ebp = 0;
    esi = 0;
    edi = 0;
}

void mach_init_mem(void)
{
    memset(_ram, 0, _ram_size);

    ////////////////////
    // From zsnes.asm

    // Yeah, these should be 1 to enable voices. :(
    m_byte(Voice0Disable) = ((1 << 0) & init.voice_disables) == 0;
    m_byte(Voice1Disable) = ((1 << 1) & init.voice_disables) == 0;
    m_byte(Voice2Disable) = ((1 << 2) & init.voice_disables) == 0;
    m_byte(Voice3Disable) = ((1 << 3) & init.voice_disables) == 0;
    m_byte(Voice4Disable) = ((1 << 4) & init.voice_disables) == 0;
    m_byte(Voice5Disable) = ((1 << 5) & init.voice_disables) == 0;
    m_byte(Voice6Disable) = ((1 << 6) & init.voice_disables) == 0;
    m_byte(Voice7Disable) = ((1 << 7) & init.voice_disables) == 0;

    ////////////////////
    // From dspproc.asm

    for (size_t i = 0; i < 65472; i++)
        m_byte(spcRam + i) = 0xFF;

    {
        uint8_t data[] = {
            0xCD,0xEF,0xBD,0xE8,0x00,0xC6,0x1D,0xD0,0xFC,0x8F,0xAA,0xF4,0x8F,0xBB,0xF5,0x78,
            0xCC,0xF4,0xD0,0xFB,0x2F,0x19,0xEB,0xF4,0xD0,0xFC,0x7E,0xF4,0xD0,0x0B,0xE4,0xF5,
            0xCB,0xF4,0xD7,0x00,0xFC,0xD0,0xF3,0xAB,0x01,0x10,0xEF,0x7E,0xF4,0x10,0xEB,0xBA,
            0xF6,0xDA,0x00,0xBA,0xF4,0xC4,0xF4,0xDD,0x5D,0xD0,0xDB,0x1F,0x00,0x00,0xC0,0xFF,
            0xAA,0xBB,0xCC,0xDD,0xEE,0xFF,0x00,0x11,0x22,0x33,0x44,0x55,0x66,0x77,0x88,0x99
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_byte(spcRom + i) = data[i];
    }

    m_dword(Filter + 0 * 4) = 0;
    m_dword(Filter + 1 * 4) = 0;
    m_dword(Filter + 2 * 4) = 240;
    m_dword(Filter + 3 * 4) = 0;
    m_dword(Filter + 4 * 4) = 488;
    m_dword(Filter + 5 * 4) = -240;
    m_dword(Filter + 6 * 4) = 460;
    m_dword(Filter + 7 * 4) = -208;

    m_dword(spcS) = 0x1FF;
    m_dword(SoundQuality) = 3;
    m_dword(SBToSPC) = 44100;
    m_byte(SoundSpeeds + 0) = 131;             // 8khz
    m_byte(SoundSpeeds + 1) = 165;             // 11khz
    m_byte(SoundSpeeds + 2) = 210;             // 22khz
    m_byte(SoundSpeeds + 3) = 233;             // 44khz
    m_dword(SBToSPCSpeeds + 0 * 4) = 8000;
    m_dword(SBToSPCSpeeds + 1 * 4) = 11025;
    m_dword(SBToSPCSpeeds + 2 * 4) = 22050;
    m_dword(SBToSPCSpeeds + 3 * 4) = 44100;

    {
        uint8_t data[] = {
            0,2,4,6,8,10,12,14,16,18,
            20,22,24,26,28,30,32,34,36,38,
            40,42,44,46,48,50,52,54,56,58,
            60,62,64,66,68,70,72,74,76,78,
            80,82,84,86,88,90,92,94,96,98,
            100,102,104,106,108,110,112,114,116,118,
            120,122,124,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,126,126,126,126,126,126,126,
            126,126,126,124,122,120,118,116,114,112,
            110,108,106,104,102,100,98,96,94,92,
            90,88,86,84,82,80,78,76,74,72,
            70,68,66,64,62,60,58,56,54,52,
            50,48,46,44,42,40,38,36,34,32,
            30,28,26,24,22,20,18,16,14,12,
            10,8,6,4,2,0
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_byte(VolumeTable + i) = data[i];
    }

    m_dword(Voice0Attack) = 1;
    m_dword(Voice1Attack) = 1;
    m_dword(Voice2Attack) = 1;
    m_dword(Voice3Attack) = 1;
    m_dword(Voice4Attack) = 1;
    m_dword(Voice5Attack) = 1;
    m_dword(Voice6Attack) = 1;
    m_dword(Voice7Attack) = 1;

    m_dword(Voice0Decay) = 1;
    m_dword(Voice1Decay) = 1;
    m_dword(Voice2Decay) = 1;
    m_dword(Voice3Decay) = 1;
    m_dword(Voice4Decay) = 1;
    m_dword(Voice5Decay) = 1;
    m_dword(Voice6Decay) = 1;
    m_dword(Voice7Decay) = 1;

    {
        uint32_t data[] = {
            45202,28665,16537,11025,7056,4189,2866,1764,1058,705,441,
            264,176,110,66,1
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(AttackRate + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            13230,8158,4851,3197,1984,1212,815,407
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(DecayRate + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            0xFFFFFFFF,418950,308700,265600,209475,154350,132300,
            103635,78277,65047,51817,38587,31972,26460,19845,16537,
            13230,9702,8158,6504,4851,4079,3197,2425,
            1984,1653,1212,1014,815,606,407,308
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(SustainRate + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            15,31,47,63,89,95,111,127
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_byte(SustainValue + i) = data[i];
    }

    {
        uint32_t data[] = {
            0xFFFFFFFF,45202,34177,28665,22050,16537,14332,11025,
            8489,7056,5622,4189,3528,2866,2094,1764,
            1433,1058,882,705,529,441,352,264,
            220,176,132,110,88,66,44,22
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(Increase + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            0xFFFFFFFF,79380,59535,50715,38587,28665,25357,19845,
            14332,12127,9922,7386,6394,4961,3748,3087,
            2425,1874,1543,1212,926,771,617,463,
            385,308,231,198,154,121,77,38
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(IncreaseBent + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            0xFFFFFFFF,45202,34177,28665,22050,16537,14332,11025,
            8489,7056,5622,4189,3528,2866,2094,1764,
            1433,1058,882,705,529,441,352,264,
            220,176,132,110,88,66,44,22
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(Decrease + i * 4) = data[i];
    }

    {
        uint32_t data[] = {
            0xFFFFFFFF,418950,308700,264600,209470,154350,132300,103635,
            78277,65047,51817,38587,31972,26460,19845,16537,
            13230,9702,8158,6504,4851,4079,3197,2425,
            1984,1653,1212,1014,815,606,407,198
        };
        for (size_t i = 0; i < sizeof(data) / sizeof(data[0]); i++)
            m_dword(DecreaseRateExp + i * 4) = data[i];
    }
}

void mach_dump_mem(const char* path)
{
    int fd = open(path, O_WRONLY | O_CREAT | O_TRUNC, 0666);
    assert(fd != -1);
    write(fd, _ram, _ram_size);
    close(fd);
}

int mach_fwrite_membuf(FILE* f, unsigned addr, size_t len)
{
    return fwrite(&_ram[addr], 1, len, f);
}
