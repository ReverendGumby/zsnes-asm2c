# Copyright (C) 2023 David Hunter
#
# This file is part of the zsnes-asm2c project.
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

CFLAGS := $(CFLAGS) -g -O3 -flto
LDFLAGS := $(LDFLAGS) -g -flto

ASMS := $(wildcard *.asm)

all: spcplay

spcplay: spcplay.o mach.o dsp.o spc700.o fnptrtab.o opcodes.o

spcplay.o: spcplay.c mach.h asm2c.h symtab.h dsp.h spc700.h fnptrtab.h opcodes.h
mach.o: mach.c asm2c.h symtab.h dsp.h spc700.h
fnptrtab.o: fnptrtab.c dsp.h spc700.h
dsp.o: dsp.c asm2c.h symtab.h
spc700.o: spc700.c asm2c.h symtab.h

.DELETE_ON_ERROR: dsp.c dsp.h spc700.c spc700.h

$(patsubst %.asm,%.c,$(ASMS)): %.c: %.asm asm2c.py
	python3 asm2c.py -i $< -t c -o $@
$(patsubst %.asm,%.h,$(ASMS)): %.h: %.asm asm2c.py
	python3 asm2c.py -i $< -t h -o $@

skipsym.txt: zsnes.sym $(ASMS)
	grep -h ^NEWSYM *.asm | sed -E 's/NEWSYM +([^ ]+).*/\1/' >skipsym.txt
	grep -h "^;;; asm2c:EXTERN" *.asm | sed -E 's/.*:EXTERN +([^ ]+).*/\1/' >>skipsym.txt

symtab.h: sym2h.py zsnes.sym skipsym.txt
	python3 sym2h.py -i zsnes.sym --skip skipsym.txt -o symtab.h

.PHONY: clean
clean: check-clean
	$(RM) dsp.c dsp.h
	$(RM) spc700.c spc700.h
	$(RM) symtab.h skipsym.txt
	$(RM) *.o
	$(RM) spcplay

.PHONY: check
check: symtab.h
	$(MAKE) -C tests

.PHONY: check-clean
check-clean:
	$(MAKE) -C tests clean
