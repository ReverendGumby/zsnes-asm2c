/*
 * Copyright (C) 2023 David Hunter
 *
 * This file is part of the zsnes-asm2c project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>

#include "mach.h"
#include "asm2c.h"
#include "dsp.h"
#include "spc700.h"
#include "fnptrtab.h"
#include "opcodes.h"

// To play the file:
//   play -b 16 -r 44100 -c 1 -L -e signed-integer play.raw

#define APPNAME                 "spcplay"

typedef struct {
    int duration_sec;
    int fade_ms;

    struct smp_regs {
        uint16_t pc;
        uint8_t ac, x, y, p, s;
    } smp_regs;
    uint8_t ram[0x10000];
    uint8_t dsp_regs[0x80];
}  song_t;

song_t song;
int cpu_print = 0;

#define TICK_HZ                 1024000

#define TICKS_PER_OPC           3                    // CPU speed
#define TICKS_PER_TIMER         (TICK_HZ / 8000 / 2) // / 2 for timrcall toggle

// SBHandler16 fills DSPBuffer with (BufferSize * 2) == 320 samples
// (16-bit). The sample rate is SBToSPCSpeeds[SoundQuality] == 11025Hz.
#define TICKS_PER_SBH           (TICK_HZ * 320 / 44100)

void read_song(const char* path, song_t* psong)
{
    uint8_t tmp[8];

    struct smp_regs* sr = &psong->smp_regs;
    FILE* f = fopen(path, "rb");
    if (f == NULL) {
        fprintf(stderr, "%s: %s\n", path, strerror(errno));
        exit(1);
    }
    fseek(f, 0x25, SEEK_SET);
    fread(tmp, 1, 2, f);
    sr->pc = (tmp[1] << 8) | tmp[0];
    fread(tmp, 1, 5, f);
    sr->ac = tmp[0];
    sr->x = tmp[1];
    sr->y = tmp[2];
    sr->p = tmp[3];
    sr->s = tmp[4];

    fseek(f, 0xa9, SEEK_SET);
    fread(tmp, 1, 3, f);
    tmp[3] = 0;
    psong->duration_sec = atoi((const char*)tmp);
    fread(tmp, 1, 5, f);
    tmp[5] = 0;
    psong->fade_ms = atoi((const char*)tmp);

    fseek(f, 0x100, SEEK_SET);
    fread(psong->ram, 1, 0x10000, f);
    fread(psong->dsp_regs, 1, 0x80, f);

    fclose(f);
}

void load_song(song_t* psong)
{
    // Load SPU RAM
    memcpy(&m_byte(spcRam), psong->ram, 0x10000);

    // Load DSP registers
    for (int i = 0; i < 0x80; i++) {
        if (i == 0x4C)     // could divide-by-0 if voice regs aren't initialized
            continue;
        mach_init_state();
        eax = psong->dsp_regs[i];
        ebx = i;
        dspWptr[i]();
    }
    mach_init_state();
    eax = psong->dsp_regs[0x4C];
    ebx = 0x4C;
    dspWptr[0x4C]();

    // Load SPC700 registers
    struct smp_regs* sr = &psong->smp_regs;
    m_dword(spcPCRam) = spcRam + sr->pc;
    m_byte(spcX) = sr->x;
    m_byte(spcY) = sr->y;
    m_byte(spcP) = sr->p;
    m_dword(spcS) = 0x100 + sr->s;
    m_dword(spcRamDP) = spcRam + 0x100 * ((sr->p & (1 << 5)) != 0);

    // Reference SPCSetFlagnvhzc
    uint8_t p_s = (sr->p & (1 << 7)) != 0;
    uint8_t p_z = (sr->p & (1 << 1)) != 0;
    if (p_z)
        m_byte(spcNZ) = 0x00;
    else if (p_s)
        m_byte(spcNZ) = 0x80;
    else
        m_byte(spcNZ) = 0x01;

    // Load SMP peripheral registers
    for (int i = 0xF0; i < 0xFD; i++) {
        mach_init_state();
        eax = psong->ram[i];
        ebx = i;
        // Avoid clearing (inbound) Port 0-3
        if (i == 0xF1)                          // Control Register
            eax &= ~0x30;                       // Port Clear PC32,PC10
        spcWptr[i - 0xF0]();
    }
}

void exec(void)
{
    static int calls = 0;

    mach_init_state();
    ebp = m_dword(spcPCRam);

    uint32_t ir = m_byte(ebp);
    if (cpu_print && ++calls == cpu_print) {
        calls = 0;
        uint32_t pc = ebp - spcRam;
        const char* opcs = opcode_st[ir];
        uint8_t ac = m_byte(spcA);
        uint8_t x = m_byte(spcX);
        uint8_t y = m_byte(spcY);
        printf("($%04x) a=$%02x x=$%02x y=$%02x ir=$%02x %s\n",
               pc, ac, x, y, ir, opcs);
    }
    ebp++;
    //ebx = 0;
    opcjmptab[ir]();

    m_dword(spcPCRam) = ebp;
}

void usage(void)
{
    printf("usage: " APPNAME " [options] spc-file\n");
    printf("    -C COUNT     print SPC700 state every COUNT instructions\n");
    printf("    -E MASK      enable only MASK voices (default: 0xFF)\n");
    printf("    -t TICKS     exit after TICKS ticks (rate %d Hz)\n", TICK_HZ);
    printf("    -o NAME      output to file NAME (default: play.raw)\n");
    printf("    -c           continuous playback (ie ignore .spc duration)\n");
}

int main(int argc, char *argv[])
{
    mach_init_t mi = {
        .voice_disables = 0x0,
    };
    int end_ticks = 0;
    char* fnout = "play.raw";
    int continuous = 0;

    int c;
    while ((c = getopt(argc, argv, "C:E:t:o:c")) != -1) {
        switch (c) {
        case 'C':
            cpu_print = strtol(optarg, NULL, 0);
            break;
        case 'E': {
            uint8_t en = strtol(optarg, NULL, 0);
            mi.voice_disables = ~en;
            break;
        }
        case 't':
            end_ticks = strtol(optarg, NULL, 0);
            break;
        case 'o':
            fnout = optarg;
            break;
        case 'c':
            continuous = 1;
            break;
        case '?':
        default:
            usage();
            exit(0);
        }
    }
    argc -= optind;
    argv += optind;

    if (argc < 1) {
        fprintf(stderr, APPNAME ": spc-file missing\n");
        usage();
        exit(0);
    }

    const char* fnin = argv[0];
    read_song(fnin, &song);

    if (!continuous)
        end_ticks = song.duration_sec * TICK_HZ;

    mach_init(&mi);
    mach_init_state();
    mach_init_mem();

    InitSPC();
    
    load_song(&song);

    int ticks = 0;
    int tmr_opc = 0;
    int tmr_timer = 0;
    int tmr_sbh = 0;

    FILE* sfout = fopen(fnout, "wb");
    if (sfout == NULL) {
        fprintf(stderr, "%s: %s\n", fnout, strerror(errno));
        exit(1);
    }

    printf("Input file: %s\n", fnin);
    printf("Output file: %s\n", fnout);

    for (;;) {
        if (++tmr_opc >= TICKS_PER_OPC) {
            tmr_opc -= TICKS_PER_OPC;
            exec();
        }
        if (++tmr_timer >= TICKS_PER_TIMER) {
            tmr_timer -= TICKS_PER_TIMER;
            //printf("updatetimer()\n");
            updatetimer();
        }
        if (++tmr_sbh >= TICKS_PER_SBH) {
            tmr_sbh -= TICKS_PER_SBH;
            //printf("SBHandler16()\n");
            SBHandler16();
            mach_fwrite_membuf(sfout, DSPBuffer, BufferSize * 4);
        }

        if (end_ticks && ++ticks == end_ticks)
            break;
        if ((ticks % (TICK_HZ * 10)) == 0) {
            printf("Progress: %ds...\r", ticks / TICK_HZ);
            fflush(stdout);
        }
    }

    return 0;
}
