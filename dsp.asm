;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;Copyright (C) 2023 David Hunter
;
;This file is part of the zsnes-asm2c project.
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

;;; asm2c:CLINE #include <stdio.h>

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; From cpu/dspproc.asm

NEWSYM conv2speed               ;000CD3BE
.next
    mov eax,[esi]
    mov ebx,[SBToSPC]
    mul ebx
    mov ebx,11025
    div ebx
    mov [esi],eax
    add esi,4
    dec ecx
    jnz .next
    ret
;;; asm2c:ENDSYM


NEWSYM InitSPC                  ;000CD3DB
      push eax
      push ebx
      push ecx
      push edx

        mov ecx,[SoundQuality]
        and ecx,3
        mov eax,dword [SBToSPCSpeeds+ecx*4]
      mov [SBToSPC],eax

      mov esi,AttackRate
      mov ecx,16
      call conv2speed
      mov esi,DecayRate
      mov ecx,8
      call conv2speed
      mov esi,SustainRate+4
      mov ecx,31
      call conv2speed
      mov esi,Increase+4
      mov ecx,31
      call conv2speed
      mov esi,IncreaseBent+4
      mov ecx,31
      call conv2speed
      mov esi,Decrease+4
      mov ecx,31
      call conv2speed
      mov esi,DecreaseRateExp+4
      mov ecx,31
      call conv2speed

      mov ecx,32768
      mov edx,32767
.nextvol
      mov al,dh
      imul dl
      shr ax,7
      mov [VolumeConvTable+edx],al
      dec edx
      dec ecx
      jnz .nextvol

      xor eax,eax               ;000CD47C
      xor ebx,ebx
      mov ebp,spcRam
        mov ebx,ebp
        add ebx,0FFFEh
        mov ax,[ebx]
        add ebp,eax
      mov [spcPCRam],ebp
      mov dword [spcS],1FFh
      mov dword [spcRamDP],spcRam
;;; asm2c:ENDSYM


%macro ProcessA 0
        mov al,[esi]
        mov cl,[dword bshift]
    shr al,4
%endmacro

%macro ProcessB 0
        mov cl,[dword bshift]
        mov al,[esi]
    and al,0Fh
%endmacro

%macro ProcessSample 1
    mov eax,[prev0]
    mov ebx,[filter0]
    imul eax,ebx
        mov edx,eax
        xor eax,eax

    mov eax,[prev1]
    mov ebx,[filter1]
    imul eax,ebx
    add edx,eax
        xor eax,eax
        sar edx,8

    %1
    test al,08h
    jz %%noneg
    or eax,0FFFFFFF0h
%%noneg
    shl eax,cl
        add edx,eax

    cmp edx,-32768
    jnl %%notless
    mov edx,-32768
%%notless
    cmp edx,32767
    jng %%notgreater
    mov edx,32767
%%notgreater
        mov eax,[prev0]
        mov [edi],dx
    mov [prev0],edx
    mov [prev1],eax
        add edi,2
%endmacro

NEWSYM BRRDecode                      ;000CF5E7
;;; asm2c:CLINE unsigned start_edi = edi;
;;; asm2c:CLINE if (0) {
;;; asm2c:CLINE     printf("BRRDecode: ESI=spcRam+%04x EDI=spcBuffer+%04x\n", esi - spcRam, edi - spcBuffer);
;;; asm2c:CLINE }

    mov byte[lastbl],0
    mov byte[loopbl],0

    xor eax,eax
    mov al,[esi]
    test al,01h
    jz .nolast
    mov byte[lastbl],1
    test al,02h
    jz .nolast
    mov byte[loopbl],1
.nolast
    inc esi
    mov cl,al
    and al,0Ch
    mov ebx,[Filter+eax*2]
    shr cl,4
    mov [filter0],ebx
    mov ebx,[Filter+eax*2+4]
    mov [bshift],cl
    mov [filter1],ebx
    mov byte[sampleleft],8

        xor ebx,ebx
        xor eax,eax
.nextsample
    ProcessSample ProcessA
    ProcessSample ProcessB
    inc esi
    dec byte[sampleleft]
    jnz near .nextsample

;;; asm2c:CLINE if (0) {
;;; asm2c:CLINE     printf("\t");
;;; asm2c:CLINE     for (int i = 0; i < 8 * 2; i++)
;;; asm2c:CLINE         printf("%04x ", m_word(start_edi + i * 2));
;;; asm2c:CLINE     printf("\n");
;;; asm2c:CLINE }
    ret
;;; asm2c:ENDSYM


NEWSYM PrepareSaveState         ;000D02C0
    push edi
    mov edi,[Voice0BufPtr]
    sub edi,spcBuffer
    mov [Voice0BufPtr],edi
    mov edi,[Voice1BufPtr]
    sub edi,spcBuffer
    mov [Voice1BufPtr],edi
    mov edi,[Voice2BufPtr]
    sub edi,spcBuffer
    mov [Voice2BufPtr],edi
    mov edi,[Voice3BufPtr]
    sub edi,spcBuffer
    mov [Voice3BufPtr],edi
    mov edi,[Voice4BufPtr]
    sub edi,spcBuffer
    mov [Voice4BufPtr],edi
    mov edi,[Voice5BufPtr]
    sub edi,spcBuffer
    mov [Voice5BufPtr],edi
    mov edi,[Voice6BufPtr]
    sub edi,spcBuffer
    mov [Voice6BufPtr],edi
    mov edi,[Voice7BufPtr]
    sub edi,spcBuffer
    mov [Voice7BufPtr],edi
    pop edi
    ret
;;; asm2c:ENDSYM


%macro VoiceStart 2
      push eax
      push ebx
      push edx
      mov dl,[DSPMem+04h+%1*10h]
      xor eax,eax
      xor ebx,ebx
      mov ah,[DSPMem+5Dh]
      and dx,0ffh
      shl dx,2
      add ax,dx
      mov bx,[spcRam+eax]
      mov dword[Voice0Ptr+%1*4],ebx
      xor ebx,ebx
      mov bx,[spcRam+eax+2]
      mov dword[Voice0LoopPtr+%1*4],ebx

      mov ax,[DSPMem+02h+%1*10h]
      cmp word[Voice0Pitch+%1*2],ax
      je .nopitchc
      mov word[Voice0Pitch+%1*2],ax
      xor ebx,ebx
      xor edx,edx
      and eax,03FFFh
      shl eax,12h
      mov ebx,[SBToSPC]
      div ebx
      shl eax,1
      mov [Voice0Freq+%1*4],eax
.nopitchc
      mov byte[Voice0Status+%1],1
      pop edx
      pop ebx
      pop eax
      mov byte [dword 0x52937+%1],0x32 ;xxx symbol?
      mov dword [Voice0Time+%1*4],0

      ; Check if adsr or gain
      test byte[DSPMem+05h+%1*10h],80h
      jnz near .adsr

        mov byte [Voice0State+%1],0 ; Gain
        push eax
        push ebx
        push edx
        xor ebx,ebx
        xor eax,eax
        and byte [DSPMem+08h+%1*10h],03Fh
        mov al,[DSPMem+08h+%1*10h]
        shl al,1
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc=0\n", __LINE__);
        mov dword [Voice0EnvInc+%1*4],0
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc+2=al=%x\n", al, __LINE__);
        mov [Voice0EnvInc+%1*4+2],al
        mov ebx,[Voice0GainTime+%1*4]
        cmp ebx,0FFFFFFFFh
        jz %%GainInfiniteTime
        cmp byte [Voice0GainType+%1],2
        jz %%.x1
        neg al
        add al,07Fh
        shl eax,16
        xor edx,edx
        mov [Voice0Time+%1*4],ebx
        div ebx
        mov [Voice0IncNumber+%1*4],eax
        pop edx
        pop ebx
        pop eax
        ret
%%.x1:
        shl eax,16
        xor edx,edx
        mov [Voice0Time+%1*4],ebx
        div ebx
        neg eax
        mov [Voice0IncNumber+%1*4],eax
        pop edx
        pop ebx
        pop eax
        ret
%%GainInfiniteTime:
        mov dword [Voice0Time+%1*4],0FFFFFFFFh
        mov dword [Voice0IncNumber+%1*4],0h
        pop edx
        pop ebx
        pop eax
        ret

.adsr:
        mov byte [Voice0State+%1],1 ; Attack
        push eax
        push ebx
        push edx

        mov ebx,[Voice0Attack+%1*4]
        mov [Voice0Time+%1*4],ebx
        xor edx,edx
        mov eax,800000h
        div ebx
        mov [Voice0IncNumber+%1*4],eax
        xor edx,edx
        xor eax,eax
        mov al,80h
        sub al,[Voice0SustainL+%1]
        shl eax,16
        mov ebx,[Voice0Decay+%1*4]
        mov [Voice0SLenNumber+%1*4],ebx
        div ebx
        neg eax
        xor eax,eax
        mov [Voice0DecreaseNumber+%1*4],eax
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc=0\n", __LINE__);
        mov dword [Voice0EnvInc+%1*4],0
        mov eax,[Voice0SustainR+%1*4]
        mov [Voice0SustainR2+%1*4],eax
        mov al,[Voice0SustainL+%1]
        mov [Voice0SustainL2+%1],al
        xor eax,eax
        mov al,[Voice0SustainL+%1]
        shl eax,16
        mov ebx,07Dh
        mul ebx
        xor edx,edx
        mov ebx,[SBToSPC]
        div ebx
        mov [Voice0SEndNumber+%1*4],eax
        mov eax,[SBToSPC]
        mov ebx,07Dh
        xor edx,edx
        div ebx
        mov [Voice0SEndLNumber+%1*4],eax

      pop edx
      pop ebx
      pop eax
      ret
%endmacro

NEWSYM Voice0Start                                      ;000D83E6
      VoiceStart 0,1
      ret
;;; asm2c:ENDSYM

NEWSYM Voice1Start                                      ;000D85A5
      VoiceStart 1,2
      ret
;;; asm2c:ENDSYM

NEWSYM Voice2Start
      VoiceStart 2,4
      ret
;;; asm2c:ENDSYM

NEWSYM Voice3Start
      VoiceStart 3,8
      ret
;;; asm2c:ENDSYM

NEWSYM Voice4Start
      VoiceStart 4,16
      ret
;;; asm2c:ENDSYM

NEWSYM Voice5Start
      VoiceStart 5,32
      ret
;;; asm2c:ENDSYM

NEWSYM Voice6Start
      VoiceStart 6,64
      ret
;;; asm2c:ENDSYM

NEWSYM Voice7Start                                      ;000D901F
      VoiceStart 7,128
      ret
;;; asm2c:ENDSYM

%macro NonEchoMono 1
    xor eax,eax
    mov ah,[Voice0Volume+%1]
    mov al,[Voice0EnvInc+%1*4+2]
    xor ch,ch
    mov edx,[BRRPlace0+%1*6+2]
    mov cl,[VolumeConvTable+eax]
    mov ax,[edi+edx*2]
;;; asm2c:CLINE { uint16_t tax = ax;
    imul cx
;;; asm2c:CLINE if (0) printf("%04x*%04x=%04x%04x\n", tax,cx,dx,ax);
;;; asm2c:CLINE }
    shr ax,7
    shl dl,1
    or ah,dl
    mov cx,[DSPBuffer+esi]
    add ax,cx
    jno %%ClipDone
    js %%ClipPos
    mov ax,-32768
    jmp %%ClipDone
%%ClipPos
    mov ax,32767
%%ClipDone
    cmp byte[Voice0Noise+%1],2 ; Wut?
    jz %%AccumDone
    mov [DSPBuffer+esi],ax
%%AccumDone
    add esi,2
    add [BRRPlace0+%1*6],ebx
%endmacro

%macro ProcessVoiceHandler16 3
    cmp byte [Voice0Disable+%1],1
    jne near %%ProcessVoiceDone
    cmp byte [Voice0Status+%1],1
    jne near %%ProcessVoiceDone
    jmp %%SkipProcess
%%ProcessNextEnvelope
    cmp byte [Voice0State+%1],1
    je %%Decay
    cmp byte [Voice0State+%1],2
    je %%Sustain
    cmp byte [Voice0State+%1],3
    je near %%Blank
    cmp byte [Voice0State+%1],4
    je near %%EndofSamp
%%MuteGain
    mov dword[Voice0Time+%1*4],0FFFFFFFFh
    mov dword[Voice0IncNumber+%1*4],0
    jmp %%EndofProcessNEnv
%%Decay
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc=0x07f0000\n", __LINE__);
    mov dword[Voice0EnvInc+%1*4],07F0000h
    mov eax,[Voice0SLenNumber+%1*4]
    mov [Voice0Time+%1*4],eax
    mov eax,[Voice0DecreaseNumber+%1*4]
    mov [Voice0IncNumber+%1*4],eax
    mov byte [Voice0State+%1],2
    jmp %%EndofProcessNEnv
%%Sustain
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc=0\n", __LINE__);
    mov dword[Voice0EnvInc+%1*4],0
    mov al,[Voice0SustainL2+%1]
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc+2=al=%x\n", __LINE__, al);
    mov [Voice0EnvInc+%1*4+2],al
    mov eax,[Voice0SustainR2+%1*4]
    mov [Voice0Time+%1*4],eax
    mov dword[Voice0IncNumber+%1*4],0
    mov byte [Voice0State+%1],3
    jmp %%EndofProcessNEnv
%%Blank
    mov eax,[Voice0SEndNumber+%1*4]
    mov [Voice0DecreaseNumber+%1*4],eax
    mov eax,[Voice0SEndLNumber+%1*4]
    mov [Voice0Time+%1*4],eax
    mov byte [Voice0State+%1],4
    jmp %%EndofProcessNEnv
%%EndofSamp
    mov byte [Voice0Status+%1],0
    jmp %%EndofProcessNEnv

%%SkipProcess
    xor eax,eax
    xor ebx,ebx
    mov al,[DSPMem+00h+%1*10h]
    mov al,[VolumeTable+eax]
    mov bl,[DSPMem+01h+%1*10h]
    mov bl,[VolumeTable+ebx]
    cmp bl,al
    ja %%LeftWins
    mov bl,al
%%LeftWins
    mov [Voice0Volume+%1],bl
    cmp byte[Voice0Volume+%1],0
    jz near %%ProcessVoiceDone
    mov byte[lastbl],0

    xor esi,esi
    mov byte[loopbl],0
    mov ebx,[Voice0Freq+%1*4]
    mov edi,[Voice0BufPtr+%1*4]

%%NextSample
    cmp dword[BRRPlace0+%1*6],100000h
    jae near %%ProcessBRR
    mov eax,[Voice0IncNumber+%1*4]
;;; asm2c:CLINE if (0) printf("%d Voice0EnvInc += eax(%x)\n", eax, __LINE__);
    add [Voice0EnvInc+%1*4],eax
    dec dword[Voice0Time+%1*4]
    jz near %%ProcessNextEnvelope
%%EndofProcessNEnv
    NonEchoMono %1
    cmp esi,BufferSize*4
    jne near %%NextSample
;;; asm2c:CLINE if (0) printf("\n");
    mov al,[Voice0EnvInc+%1*4+2]
    shr al,1
    mov [DSPMem+08h+%1*10h],al
    jmp %%ProcessVoice1

%%ProcessBRR
    cmp byte [Voice0End+%1],1
    je near %%noDecode1Block
%%Decode1Block
    sub dword [BRRPlace0+%1*6],100000h
    push esi
    mov esi, dword [Voice0Ptr+%1*4]
    mov eax,[spcRam+esi]
    mov ebx,[spcRam+esi+4]
    cmp eax,[spcRamcmp+esi]
    jne near %%looped
    cmp ebx,[spcRamcmp+esi+4]
    jne near %%looped
    mov eax,[spcPrevbf+esi]
    mov ebx,[spcPrevbf+esi+4]
    cmp eax,[Voice0Prev0+%1*4]
    jne short %%looped
    cmp ebx,[Voice0Prev1+%1*4]
    jne short %%looped
    mov al,[spcRam+esi+8]
    cmp al,[spcRamcmp+esi+8]
    jne short %%looped
    mov byte[Voice0End+%1],0
    mov byte[Voice0Loop+%1],0
    test byte[spcRam+esi],01h
    jz short %%nolast
    test byte[spcRam+esi],02h
    jnz short %%looped
    mov byte[Voice0End+%1],1
%%nolast
    mov eax,[spcPrevbf+esi+9]
    mov ebx,[spcPrevbf+esi+13]
    mov [Voice0Prev0+%1*4],eax
    mov [Voice0Prev1+%1*4],ebx
    mov edi,esi
    inc edi
    add dword [Voice0Ptr+%1*4],9
    shl edi,2
    pop esi
    add edi,spcBuffer
    mov ebx,[Voice0Freq+%1*4]
    mov [Voice0BufPtr+%1*4],edi
    jmp %%NextSample
%%looped
    mov eax,[Voice0Prev0+%1*4]
    mov ebx,[Voice0Prev1+%1*4]
    mov [spcPrevbf+esi],eax
    mov [spcPrevbf+esi+4],ebx
%%convertBRR
    mov eax,[spcRam+esi]
    mov edi,esi
    mov ebx,[spcRam+esi+4]
    mov [spcRamcmp+esi],eax
    inc edi
    mov [spcRamcmp+esi+4],ebx
    mov al,[spcRam+esi+8]
    shl edi,2
    mov [spcRamcmp+esi+8],al
%%convertBRR2
    add esi,spcRam
    add edi,spcBuffer
    mov eax,[Voice0Prev0+%1*4]
    mov [Voice0BufPtr+%1*4],edi
    mov dword [prev0],eax
    mov eax,[Voice0Prev1+%1*4]
    mov dword [prev1],eax
    call BRRDecode
    pop esi
    mov eax,dword [prev0]
    mov edi,[Voice0BufPtr+%1*4]
    mov [Voice0Prev0+%1*4],eax
    mov eax,dword [prev1]
    mov [Voice0Prev1+%1*4],eax
    mov al,[loopbl]
    mov [Voice0Loop+%1],al
    mov al,[lastbl]
    mov [Voice0End+%1],al
    mov ebx,[Voice0Freq+%1*4]
    add dword [Voice0Ptr+%1*4],9
    jmp %%NextSample
%%noDecode1Block
    or byte [DSPMem+7Ch],%2
    and byte [DSPMem+5Ch],%3
    and byte [DSPMem+4Ch],%3
    cmp byte [Voice0Loop+%1],1
    jne near %%EndSample
    mov eax,[Voice0LoopPtr+%1*4]
    sub eax,9
    mov [Voice0Ptr+%1*4],eax
    mov eax,[Voice0Prev0+%1*4]
    mov [Voice0Prev1+%1*4],eax
    jmp %%Decode1Block
%%EndSample
    mov byte [Voice0Status+%1],0
    jmp %%ProcessVoiceDone
%%ProcessVoice1
    mov ax,[DSPMem+02h+%1*10h]
    cmp word[Voice0Pitch+%1*2],ax
    je %%nopitchc
    mov word[Voice0Pitch+%1*2],ax
    xor ebx,ebx
    and eax,03FFFh
    xor edx,edx
    shl eax,12h
    mov ebx,[SBToSPC]
    div ebx
    shl eax,1
    mov [Voice0Freq+%1*4],eax
%%nopitchc
%%ProcessVoiceDone
%endmacro

%macro ProcessSoundBuffer 0
    ; Clear the DSP Buffer
    mov edi,DSPBuffer
    xor eax,eax
    mov ecx,BufferSize
.bufloop
    mov dword[edi],0
    add edi,4
    dec ecx
    jnz .bufloop

    ProcessVoiceHandler16 0,1,254
    ProcessVoiceHandler16 1,2,253
    ProcessVoiceHandler16 2,4,251
    ProcessVoiceHandler16 3,8,247
    ProcessVoiceHandler16 4,16,239
    ProcessVoiceHandler16 5,32,223
    ProcessVoiceHandler16 6,64,191
    ProcessVoiceHandler16 7,128,127
%endmacro
        
NEWSYM SBHandler16                              ;000DB220
    ; Process the sound :I

    ProcessSoundBuffer

    ret
;;; asm2c:ENDSYM


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; From cpu/dsp.asm

;Read DSP Registers functions

NEWSYM RDSPReg00       ; Voice  0  Volume Left  ;000E2894
      mov al,[DSPMem+00h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg01       ; Voice  0  Volume Right
      mov al,[DSPMem+01h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg02       ; Voice  0  Pitch Low
      mov al,[DSPMem+02h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg03       ; Voice  0  Pitch High
      mov al,[DSPMem+03h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg04       ; Voice  0  SCRN
      mov al,[DSPMem+04h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg05       ; Voice  0  ADSR (1)
      mov al,[DSPMem+05h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg06       ; Voice  0  ADSR (2)
      mov al,[DSPMem+06h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg07       ; Voice  0  GAIN
      mov al,[DSPMem+07h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg08       ; Voice  0  ENVX
      mov al,[DSPMem+08h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg09       ; Voice  0  OUTX
      mov al,[DSPMem+09h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0A       ;
      mov al,[DSPMem+0Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0B       ;
      mov al,[DSPMem+0Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0C       ;
      mov al,[DSPMem+0Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0D       ;
      mov al,[DSPMem+0Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0E       ;
      mov al,[DSPMem+0Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg0F       ; Voice  0  Echo coefficient
      mov al,[DSPMem+0Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg10       ; Voice  1  Volume Left
      mov al,[DSPMem+10h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg11       ; Voice  1  Volume Right
      mov al,[DSPMem+11h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg12       ; Voice  1  Pitch Low
      mov al,[DSPMem+012h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg13       ; Voice  1  Pitch High
      mov al,[DSPMem+013h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg14       ; Voice  1  SCRN
      mov al,[DSPMem+014h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg15       ; Voice  1  ADSR (1)
      mov al,[DSPMem+015h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg16       ; Voice  1  ADSR (2)
      mov al,[DSPMem+016h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg17       ; Voice  1  GAIN
      mov al,[DSPMem+017h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg18       ; Voice  1  ENVX
      mov al,[DSPMem+018h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg19       ; Voice  1  OUTX
      mov al,[DSPMem+019h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1A       ;
      mov al,[DSPMem+01Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1B       ;
      mov al,[DSPMem+01Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1C       ;
      mov al,[DSPMem+01Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1D       ;
      mov al,[DSPMem+01Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1E       ;
      mov al,[DSPMem+01Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg1F       ; Voice  1  Echo coefficient
      mov al,[DSPMem+01Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg20       ; Voice  2  Volume Left
      mov al,[DSPMem+20h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg21       ; Voice  2  Volume Right
      mov al,[DSPMem+21h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg22       ; Voice  2  Pitch Low
      mov al,[DSPMem+022h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg23       ; Voice  2  Pitch High
      mov al,[DSPMem+023h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg24       ; Voice  2  SCRN
      mov al,[DSPMem+024h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg25       ; Voice  2  ADSR (1)
      mov al,[DSPMem+025h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg26       ; Voice  2  ADSR (2)
      mov al,[DSPMem+026h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg27       ; Voice  2  GAIN
      mov al,[DSPMem+027h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg28       ; Voice  2  ENVX
      mov al,[DSPMem+028h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg29       ; Voice  2  OUTX
      mov al,[DSPMem+029h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2A       ;
      mov al,[DSPMem+02Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2B       ;
      mov al,[DSPMem+02Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2C       ;
      mov al,[DSPMem+02Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2D       ;
      mov al,[DSPMem+02Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2E       ;
      mov al,[DSPMem+02Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg2F       ; Voice  2  Echo coefficient
      mov al,[DSPMem+02Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg30       ; Voice  3  Volume Left
      mov al,[DSPMem+30h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg31       ; Voice  3  Volume Right
      mov al,[DSPMem+31h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg32       ; Voice  3  Pitch Low
      mov al,[DSPMem+032h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg33       ; Voice  3  Pitch High
      mov al,[DSPMem+033h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg34       ; Voice  3  SCRN
      mov al,[DSPMem+034h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg35       ; Voice  3  ADSR (1)
      mov al,[DSPMem+035h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg36       ; Voice  3  ADSR (2)
      mov al,[DSPMem+036h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg37       ; Voice  3  GAIN
      mov al,[DSPMem+037h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg38       ; Voice  3  ENVX
      mov al,[DSPMem+038h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg39       ; Voice  3  OUTX
      mov al,[DSPMem+039h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3A       ;
      mov al,[DSPMem+03Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3B       ;
      mov al,[DSPMem+03Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3C       ;
      mov al,[DSPMem+03Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3D       ;
      mov al,[DSPMem+03Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3E       ;
      mov al,[DSPMem+03Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg3F       ; Voice  3  Echo coefficient
      mov al,[DSPMem+03Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg40       ; Voice  4  Volume Left
      mov al,[DSPMem+40h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg41       ; Voice  4  Volume Right
      mov al,[DSPMem+41h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg42       ; Voice  4  Pitch Low
      mov al,[DSPMem+042h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg43       ; Voice  4  Pitch High
      mov al,[DSPMem+043h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg44       ; Voice  4  SCRN
      mov al,[DSPMem+044h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg45       ; Voice  4  ADSR (1)
      mov al,[DSPMem+045h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg46       ; Voice  4  ADSR (2)
      mov al,[DSPMem+046h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg47       ; Voice  4  GAIN
      mov al,[DSPMem+047h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg48       ; Voice  4  ENVX
      mov al,[DSPMem+048h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg49       ; Voice  4  OUTX
      mov al,[DSPMem+049h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4A       ;
      mov al,[DSPMem+04Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4B       ;
      mov al,[DSPMem+04Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4C       ;
      mov al,[DSPMem+04Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4D       ;
      mov al,[DSPMem+04Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4E       ;
      mov al,[DSPMem+04Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg4F       ; Voice  4  Echo coefficient
      mov al,[DSPMem+04Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg50       ; Voice  5  Volume Left
      mov al,[DSPMem+50h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg51       ; Voice  5  Volume Right
      mov al,[DSPMem+51h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg52       ; Voice  5  Pitch Low
      mov al,[DSPMem+052h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg53       ; Voice  5  Pitch High
      mov al,[DSPMem+053h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg54       ; Voice  5  SCRN
      mov al,[DSPMem+054h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg55       ; Voice  5  ADSR (1)
      mov al,[DSPMem+055h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg56       ; Voice  5  ADSR (2)
      mov al,[DSPMem+056h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg57       ; Voice  5  GAIN
      mov al,[DSPMem+057h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg58       ; Voice  5  ENVX
      mov al,[DSPMem+058h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg59       ; Voice  5  OUTX
      mov al,[DSPMem+059h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5A       ;
      mov al,[DSPMem+05Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5B       ;
      mov al,[DSPMem+05Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5C       ;
      mov al,[DSPMem+05Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5D       ;
      mov al,[DSPMem+05Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5E       ;
      mov al,[DSPMem+05Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg5F       ; Voice  5  Echo coefficient
      mov al,[DSPMem+05Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg60       ; Voice  6  Volume Left
      mov al,[DSPMem+60h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg61       ; Voice  6  Volume Right
      mov al,[DSPMem+61h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg62       ; Voice  6  Pitch Low
      mov al,[DSPMem+062h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg63       ; Voice  6  Pitch High
      mov al,[DSPMem+063h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg64       ; Voice  6  SCRN
      mov al,[DSPMem+064h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg65       ; Voice  6  ADSR (1)
      mov al,[DSPMem+065h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg66       ; Voice  6  ADSR (2)
      mov al,[DSPMem+066h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg67       ; Voice  6  GAIN
      mov al,[DSPMem+067h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg68       ; Voice  6  ENVX
      mov al,[DSPMem+068h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg69       ; Voice  6  OUTX
      mov al,[DSPMem+069h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6A       ;
      mov al,[DSPMem+06Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6B       ;
      mov al,[DSPMem+06Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6C       ;
      mov al,[DSPMem+06Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6D       ;
      mov al,[DSPMem+06Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6E       ;
      mov al,[DSPMem+06Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg6F       ; Voice  6  Echo coefficient
      mov al,[DSPMem+06Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg70       ; Voice  7  Volume Left
      mov al,[DSPMem+70h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg71       ; Voice  7  Volume Right
      mov al,[DSPMem+71h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg72       ; Voice  7  Pitch Low
      mov al,[DSPMem+072h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg73       ; Voice  7  Pitch High
      mov al,[DSPMem+073h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg74       ; Voice  7  SCRN
      mov al,[DSPMem+074h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg75       ; Voice  7  ADSR (1)
      mov al,[DSPMem+075h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg76       ; Voice  7  ADSR (2)
      mov al,[DSPMem+076h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg77       ; Voice  7  GAIN
      mov al,[DSPMem+077h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg78       ; Voice  7  ENVX
      mov al,[DSPMem+078h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg79       ; Voice  7  OUTX
      mov al,[DSPMem+079h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7A       ;
      mov al,[DSPMem+07Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7B       ;
      mov al,[DSPMem+07Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7C       ;
      mov al,[DSPMem+07Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7D       ;
      mov al,[DSPMem+07Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7E       ;
      mov al,[DSPMem+07Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg7F       ; Voice  7  Echo coefficient
      mov al,[DSPMem+07Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg80       ;
      mov al,[DSPMem+080h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg81       ;
      mov al,[DSPMem+081h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg82       ;
      mov al,[DSPMem+082h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg83       ;
      mov al,[DSPMem+083h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg84       ;
      mov al,[DSPMem+084h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg85       ;
      mov al,[DSPMem+085h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg86       ;
      mov al,[DSPMem+086h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg87       ;
      mov al,[DSPMem+087h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg88       ;
      mov al,[DSPMem+088h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg89       ;
      mov al,[DSPMem+089h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8A       ;
      mov al,[DSPMem+08Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8B       ;
      mov al,[DSPMem+08Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8C       ;
      mov al,[DSPMem+08Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8D       ;
      mov al,[DSPMem+08Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8E       ;
      mov al,[DSPMem+08Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg8F       ;
      mov al,[DSPMem+08Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg90       ;
      mov al,[DSPMem+090h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg91       ;
      mov al,[DSPMem+091h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg92       ;
      mov al,[DSPMem+092h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg93       ;
      mov al,[DSPMem+093h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg94       ;
      mov al,[DSPMem+094h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg95       ;
      mov al,[DSPMem+095h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg96       ;
      mov al,[DSPMem+096h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg97       ;
      mov al,[DSPMem+097h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg98       ;
      mov al,[DSPMem+098h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg99       ;
      mov al,[DSPMem+099h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9A       ;
      mov al,[DSPMem+09Ah]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9B       ;
      mov al,[DSPMem+09Bh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9C       ;
      mov al,[DSPMem+09Ch]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9D       ;
      mov al,[DSPMem+09Dh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9E       ;
      mov al,[DSPMem+09Eh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPReg9F       ;
      mov al,[DSPMem+09Fh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA0       ;
      mov al,[DSPMem+0A0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA1       ;
      mov al,[DSPMem+0A1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA2       ;
      mov al,[DSPMem+0A2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA3       ;
      mov al,[DSPMem+0A3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA4       ;
      mov al,[DSPMem+0A4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA5       ;
      mov al,[DSPMem+0A5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA6       ;
      mov al,[DSPMem+0A6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA7       ;
      mov al,[DSPMem+0A7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA8       ;
      mov al,[DSPMem+0A8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegA9       ;
      mov al,[DSPMem+0A9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAA       ;
      mov al,[DSPMem+0AAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAB       ;
      mov al,[DSPMem+0ABh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAC       ;
      mov al,[DSPMem+0ACh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAD       ;
      mov al,[DSPMem+0ADh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAE       ;
      mov al,[DSPMem+0AEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegAF       ;
      mov al,[DSPMem+0AFh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB0       ;
      mov al,[DSPMem+0B0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB1       ;
      mov al,[DSPMem+0B1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB2       ;
      mov al,[DSPMem+0B2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB3       ;
      mov al,[DSPMem+0B3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB4       ;
      mov al,[DSPMem+0B4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB5       ;
      mov al,[DSPMem+0B5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB6       ;
      mov al,[DSPMem+0B6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB7       ;
      mov al,[DSPMem+0B7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB8       ;
      mov al,[DSPMem+0B8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegB9       ;
      mov al,[DSPMem+0B9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBA       ;
      mov al,[DSPMem+0BAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBB       ;
      mov al,[DSPMem+0BBh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBC       ;
      mov al,[DSPMem+0BCh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBD       ;
      mov al,[DSPMem+0BDh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBE       ;
      mov al,[DSPMem+0BEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegBF       ;
      mov al,[DSPMem+0BFh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC0       ;
      mov al,[DSPMem+0C0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC1       ;
      mov al,[DSPMem+0C1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC2       ;
      mov al,[DSPMem+0C2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC3       ;
      mov al,[DSPMem+0C3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC4       ;
      mov al,[DSPMem+0C4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC5       ;
      mov al,[DSPMem+0C5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC6       ;
      mov al,[DSPMem+0C6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC7       ;
      mov al,[DSPMem+0C7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC8       ;
      mov al,[DSPMem+0C8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegC9       ;
      mov al,[DSPMem+0C9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCA       ;
      mov al,[DSPMem+0CAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCB       ;
      mov al,[DSPMem+0CBh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCC       ;
      mov al,[DSPMem+0CCh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCD       ;
      mov al,[DSPMem+0CDh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCE       ;
      mov al,[DSPMem+0CEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegCF       ;
      mov al,[DSPMem+0CFh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD0       ;
      mov al,[DSPMem+0D0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD1       ;
      mov al,[DSPMem+0D1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD2       ;
      mov al,[DSPMem+0D2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD3       ;
      mov al,[DSPMem+0D3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD4       ;
      mov al,[DSPMem+0D4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD5       ;
      mov al,[DSPMem+0D5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD6       ;
      mov al,[DSPMem+0D6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD7       ;
      mov al,[DSPMem+0D7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD8       ;
      mov al,[DSPMem+0D8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegD9       ;
      mov al,[DSPMem+0D9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDA       ;
      mov al,[DSPMem+0DAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDB       ;
      mov al,[DSPMem+0DBh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDC       ;
      mov al,[DSPMem+0DCh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDD       ;
      mov al,[DSPMem+0DDh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDE       ;
      mov al,[DSPMem+0DEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegDF       ;
      mov al,[DSPMem+0DFh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE0       ;
      mov al,[DSPMem+0E0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE1       ;
      mov al,[DSPMem+0E1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE2       ;
      mov al,[DSPMem+0E2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE3       ;
      mov al,[DSPMem+0E3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE4       ;
      mov al,[DSPMem+0E4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE5       ;
      mov al,[DSPMem+0E5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE6       ;
      mov al,[DSPMem+0E6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE7       ;
      mov al,[DSPMem+0E7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE8       ;
      mov al,[DSPMem+0E8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegE9       ;
      mov al,[DSPMem+0E9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegEA       ;
      mov al,[DSPMem+0EAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegEB       ;
      mov al,[DSPMem+0EBh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegEC       ;
      mov al,[DSPMem+0ECh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegED       ;
      mov al,[DSPMem+0EDh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegEE       ;
      mov al,[DSPMem+0EEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegEF       ;
      mov al,[DSPMem+0EFh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF0       ;
      mov al,[DSPMem+0F0h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF1       ;
      mov al,[DSPMem+0F1h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF2       ;
      mov al,[DSPMem+0F2h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF3       ;
      mov al,[DSPMem+0F3h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF4       ;
      mov al,[DSPMem+0F4h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF5       ;
      mov al,[DSPMem+0F5h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF6       ;
      mov al,[DSPMem+0F6h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF7      ;
      mov al,[DSPMem+0F7h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF8      ;
      mov al,[DSPMem+0F8h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegF9      ;
      mov al,[DSPMem+0F9h]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFA      ;
      mov al,[DSPMem+0FAh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFB      ;
      mov al,[DSPMem+0FBh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFC      ;
      mov al,[DSPMem+0FCh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFD      ;
      mov al,[DSPMem+0FDh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFE      ;
      mov al,[DSPMem+0FEh]
      ret
;;; asm2c:ENDSYM

NEWSYM RDSPRegFF      ;
      mov al,[DSPMem+0FFh]
      ret
;;; asm2c:ENDSYM

%macro VoiceGain 1
      mov [DSPMem+07h+%1*10h],al
      push eax
      push ebx
      test byte [DSPMem+07h+%1*10h],80h
      jz near %%Direct
      test byte [DSPMem+07h+%1*10h],40h
      jnz %%Increase
      test byte [DSPMem+07h+%1*10h],20h
      jz %%LinearDec
      mov byte[Voice0GainType+%1],2 ; Decrease
      xor eax,eax
      mov al,[DSPMem+07h+%1*10h]
      and al,1Fh
      mov ebx,[DecreaseRateExp+eax*4]
      mov [Voice0GainTime+%1*4],ebx
      jmp %%Done
%%LinearDec
      mov byte[Voice0GainType+%1],2 ; Decrease
      xor eax,eax
      mov al,[DSPMem+07h+%1*10h]
      and al,1Fh
      mov ebx,[Decrease+eax*4]
      mov [Voice0GainTime+%1*4],ebx
      jmp %%Done
%%Increase
      test byte [DSPMem+07h+%1*10h],20h
      jz %%LinearInc
      mov byte[Voice0GainType+%1],1 ; Increase
      xor eax,eax
      mov al,[DSPMem+07h+%1*10h]
      and al,1Fh
      mov ebx,[IncreaseBent+eax*4]
      mov dword[Voice0GainTime+%1*4],ebx
      jmp %%Done
%%LinearInc
      mov byte[Voice0GainType+%1],1 ; Increase
      xor eax,eax
      mov al,[DSPMem+07h+%1*10h]
      and al,1Fh
      mov ebx,[Increase+eax*4]
      mov dword[Voice0GainTime+%1*4],ebx
      jmp %%Done
%%Direct
      mov dword [Voice0GainTime+%1*4],0FFFFFFFFh
      mov byte[Voice0GainType+%1],0 ; Direct
      mov al,[DSPMem+07h+%1*10h]
      and al,3Fh
      mov [DSPMem+08h+%1*10h],al
%%Done
      pop ebx
      pop eax
%endmacro


%macro VoiceAdsr 1
      push eax
      push ebx
      xor eax,eax
      mov al,[DSPMem+05h+%1*10h]
      and al,0Fh
      mov ebx,dword [AttackRate+eax*4]
      mov dword[Voice0Attack+%1*4],ebx
      mov al,[DSPMem+05h+%1*10h]
      shr al,4
      and al,07h
      mov ebx,[DecayRate+eax*4]
      mov [Voice0Decay+%1*4],ebx
      mov al,[DSPMem+06h+%1*10h]
      and al,1Fh
      mov ebx,[SustainRate+eax*4]
      mov [Voice0SustainR+%1*4],ebx
      mov al,[DSPMem+06h+%1*10h]
      shr al,5
      and al,07h
      mov bl,[SustainValue+eax]
      mov [Voice0SustainL+%1],bl
      pop ebx
      pop eax
%endmacro


;NEWSYM ADSRGAINSwitch, db 0

;Write DSP Registers functions

NEWSYM WDSPReg00       ; Voice  0  Volume Left
      mov [DSPMem+00h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg01       ; Voice  0  Volume Right
      mov [DSPMem+01h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg02       ; Voice  0  Pitch Low
      mov [DSPMem+02h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg03       ; Voice  0  Pitch High
      mov [DSPMem+03h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg04       ; Voice  0  SCRN
      mov [DSPMem+04h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg05       ; Voice  0  ADSR (1)
      mov [DSPMem+05h],al
      VoiceAdsr 0
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg06       ; Voice  0  ADSR (2)
      mov [DSPMem+06h],al
      VoiceAdsr 0
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg07       ; Voice  0  GAIN
      VoiceGain 0
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg08       ; Voice  0  ENVX
      mov [DSPMem+08h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg09       ; Voice  0  OUTX
      mov [DSPMem+09h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0A       ; Voice  0
      mov [DSPMem+0Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0B       ; Voice  0
      mov [DSPMem+0Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0C       ; Voice  0
      mov [DSPMem+0Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0D       ; Echo Feedback
      mov [DSPMem+0Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0E       ; Voice  0
      mov [DSPMem+0Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg0F       ; Voice  0  Echo coefficient
      mov [DSPMem+0Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg10       ; Voice  1  Volume Left
      mov [DSPMem+10h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg11       ; Voice  1  Volume Right
      mov [DSPMem+11h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg12       ; Voice  1  Pitch Low
      mov [DSPMem+012h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg13       ; Voice  1  Pitch High
      mov [DSPMem+013h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg14       ; Voice  1  SCRN
      mov [DSPMem+14h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg15       ; Voice  1  ADSR (1)
      mov [DSPMem+15h],al
      VoiceAdsr 1
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg16       ; Voice  1  ADSR (2)
      mov [DSPMem+16h],al
      VoiceAdsr 1
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg17       ; Voice  1  GAIN
      VoiceGain 1
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg18       ; Voice  1  ENVX
      mov [DSPMem+018h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg19       ; Voice  1  OUTX
      mov [DSPMem+019h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1A       ; Voice  1
      mov [DSPMem+01Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1B       ; Voice  1
      mov [DSPMem+01Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1C       ; Voice  1
      mov [DSPMem+01Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1D       ; Voice  1
      mov [DSPMem+01Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1E       ; Voice  1
      mov [DSPMem+01Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg1F       ; Voice  1  Echo coefficient
      mov [DSPMem+01Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg20       ; Voice  2  Volume Left
      mov [DSPMem+20h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg21       ; Voice  2  Volume Right
      mov [DSPMem+21h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg22       ; Voice  2  Pitch Low
      mov [DSPMem+022h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg23       ; Voice  2  Pitch High
      mov [DSPMem+023h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg24       ; Voice  2  SCRN
      mov [DSPMem+24h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg25       ; Voice  2  ADSR (1)
      mov [DSPMem+25h],al
      VoiceAdsr 2
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg26       ; Voice  2  ADSR (2)
      mov [DSPMem+26h],al
      VoiceAdsr 2
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg27       ; Voice  2  GAIN
      VoiceGain 2
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg28       ; Voice  2  ENVX
      mov [DSPMem+028h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg29       ; Voice  2  OUTX
      mov [DSPMem+029h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2A       ; Voice  2
      mov [DSPMem+02Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2B       ; Voice  2
      mov [DSPMem+02Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2C       ; Voice  2
      mov [DSPMem+02Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2D       ; Voice  2
      mov [DSPMem+02Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2E       ; Voice  2
      mov [DSPMem+02Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg2F       ; Voice  2  Echo coefficient
      mov [DSPMem+02Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg30       ; Voice  3  Volume Left
      mov [DSPMem+30h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg31       ; Voice  3  Volume Right
      mov [DSPMem+31h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg32       ; Voice  3  Pitch Low
      mov [DSPMem+032h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg33       ; Voice  3  Pitch High
      mov [DSPMem+033h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg34       ; Voice  3  SCRN
      mov [DSPMem+34h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg35       ; Voice  3  ADSR (1)
      mov [DSPMem+35h],al
      VoiceAdsr 3
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg36       ; Voice  3  ADSR (2)
      mov [DSPMem+36h],al
      VoiceAdsr 3
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg37       ; Voice  3  GAIN
      VoiceGain 3
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg38       ; Voice  3  ENVX
      mov [DSPMem+038h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg39       ; Voice  3  OUTX
      mov [DSPMem+039h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3A       ; Voice  3
      mov [DSPMem+03Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3B       ; Voice  3
      mov [DSPMem+03Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3C       ; Voice  3
      mov [DSPMem+03Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3D       ; Voice  3
      mov byte [Voice0Noise],0
      mov byte [Voice1Noise],0
      mov byte [Voice2Noise],0
      mov byte [Voice3Noise],0
      mov byte [Voice4Noise],0
      mov byte [Voice5Noise],0
      mov byte [Voice6Noise],0
      mov byte [Voice7Noise],0
      test al,1
      jz .TestVoice1
      mov byte [Voice0Noise],1
.TestVoice1
      test al,2
      jz .TestVoice2
      mov byte [Voice1Noise],1
.TestVoice2
      test al,4
      jz .TestVoice3
      mov byte [Voice2Noise],1
.TestVoice3
      test al,8
      jz .TestVoice4
      mov byte [Voice3Noise],1
.TestVoice4
      test al,16
      jz .TestVoice5
      mov byte [Voice4Noise],1
.TestVoice5
      test al,32
      jz .TestVoice6
      mov byte [Voice5Noise],1
.TestVoice6
      test al,64
      jz .TestVoice7
      mov byte [Voice6Noise],1
.TestVoice7
      test al,128
      jz .TestVoice8
      mov byte [Voice7Noise],1
.TestVoice8
      mov [DSPMem+03Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3E       ; Voice  3
      mov [DSPMem+03Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg3F       ; Voice  3  Echo coefficient
      mov [DSPMem+03Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg40       ; Voice  4  Volume Left
      mov [DSPMem+40h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg41       ; Voice  4  Volume Right
      mov [DSPMem+41h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg42       ; Voice  4  Pitch Low
      mov [DSPMem+042h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg43       ; Voice  4  Pitch High
      mov [DSPMem+043h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg44       ; Voice  4  SCRN
      mov [DSPMem+44h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg45       ; Voice  4  ADSR (1)
      mov [DSPMem+45h],al
      VoiceAdsr 4
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg46       ; Voice  4  ADSR (2)
      mov [DSPMem+46h],al
      VoiceAdsr 4
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg47       ; Voice  4  GAIN
      VoiceGain 4
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg48       ; Voice  4  ENVX
      mov [DSPMem+048h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg49       ; Voice  4  OUTX
      mov [DSPMem+049h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg4A       ; Voice  4
      mov [DSPMem+04Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg4B       ; Voice  4
      mov [DSPMem+04Bh],al
      ret
;;; asm2c:ENDSYM

%macro ResetVoice 1
      mov dword [Voice0Prev0+%1*4],0
      mov dword [Voice0Prev1+%1*4],0
      mov byte  [Voice0End+%1],0
;      mov byte  [Voice0Loop+%1],0
      mov dword[BRRPlace0+%1*6],100000h
%endm

NEWSYM WDSPReg4C       ; Key On
;;; asm2c:CLINE if (0) printf("Key On %02x\n", al);
;NEWSYM ProcessKeyOn
      test al,1
      jz .TestVoice1
      push edx
;      ResetVoice 0
      mov dword [Voice0Prev0],0
      mov dword [Voice0Prev1],0
      mov byte  [Voice0End],0
      mov byte  [Voice0Loop],0
      mov dword[BRRPlace0],100000h
      call Voice0Start
      pop edx
.TestVoice1
      test al,2
      jz .TestVoice2
      push edx
      ResetVoice 1
      call Voice1Start
      pop edx
.TestVoice2
      test al,4
      jz .TestVoice3
      push edx
      ResetVoice 2
      call Voice2Start
      pop edx
.TestVoice3
      test al,8
      jz .TestVoice4
      push edx
      ResetVoice 3
      call Voice3Start
      pop edx
.TestVoice4
      test al,16
      jz .TestVoice5
      push edx
      ResetVoice 4
      call Voice4Start
      pop edx
.TestVoice5
      test al,32
      jz .TestVoice6
      push edx
      ResetVoice 5
      call Voice5Start
      pop edx
.TestVoice6
      test al,64
      jz .TestVoice7
      push edx
      ResetVoice 6
      call Voice6Start
      pop edx
.TestVoice7
      test al,128
      jz .TestVoice8
      push edx
      ResetVoice 7
      call Voice7Start
      pop edx
.TestVoice8
      mov [DSPMem+04Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg4D       ; Voice  4
      mov [DSPMem+04Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg4E       ; Voice  4
      mov [DSPMem+04Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg4F       ; Voice  4  Echo coefficient
      mov [DSPMem+04Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg50       ; Voice  5  Volume Left
      mov [DSPMem+50h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg51       ; Voice  5  Volume Right
      mov [DSPMem+51h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg52       ; Voice  5  Pitch Low
      mov [DSPMem+052h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg53       ; Voice  5  Pitch High
      mov [DSPMem+053h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg54       ; Voice  5  SCRN
      mov [DSPMem+54h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg55       ; Voice  5  ADSR (1)
      mov [DSPMem+55h],al
      VoiceAdsr 5
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg56       ; Voice  5  ADSR (2)
      mov [DSPMem+56h],al
      VoiceAdsr 5
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg57       ; Voice  5  GAIN
      VoiceGain 5
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg58       ; Voice  5  ENVX
      mov [DSPMem+058h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg59       ; Voice  5  OUTX
      mov [DSPMem+059h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg5A       ; Voice  5
      mov [DSPMem+05Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg5B       ; Voice  5
      mov [DSPMem+05Bh],al
      ret
;;; asm2c:ENDSYM

%macro keyoffm 1
    and byte [DSPMem+4Ch],(0FFh - (1 << %1))
    mov byte [Voice0Status+%1],0
%endmacro

NEWSYM WDSPReg5C       ; Key Off
;;; asm2c:CLINE if (0) printf("Key Off %02x\n", al);
      test al,1
      jz .TestVoice1
      keyoffm 0
.TestVoice1
      test al,2
      jz .TestVoice2
      keyoffm 1
.TestVoice2
      test al,4
      jz .TestVoice3
      keyoffm 2
.TestVoice3
      test al,8
      jz .TestVoice4
      keyoffm 3
.TestVoice4
      test al,16
      jz .TestVoice5
      keyoffm 4
.TestVoice5
      test al,32
      jz .TestVoice6
      keyoffm 5
.TestVoice6
      test al,64
      jz .TestVoice7
      keyoffm 6
.TestVoice7
      test al,128
      jz .TestVoice8
      keyoffm 7
.TestVoice8
      mov [DSPMem+05Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg5D       ; Voice  5
      mov [DSPMem+05Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg5E       ; Voice  5
      mov [DSPMem+05Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg5F       ; Voice  5  Echo coefficient
      mov [DSPMem+05Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg60       ; Voice  6  Volume Left
      mov [DSPMem+60h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg61       ; Voice  6  Volume Right
      mov [DSPMem+61h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg62       ; Voice  6  Pitch Low
      mov [DSPMem+062h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg63       ; Voice  6  Pitch High
      mov [DSPMem+063h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg64       ; Voice  6  SCRN
      mov [DSPMem+64h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg65       ; Voice  6  ADSR (1)
      mov [DSPMem+65h],al
      VoiceAdsr 6
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg66       ; Voice  6  ADSR (2)
      mov [DSPMem+66h],al
      VoiceAdsr 6
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg67       ; Voice  6  GAIN
      VoiceGain 6
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg68       ; Voice  6  ENVX
      mov [DSPMem+068h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg69       ; Voice  6  OUTX
      mov [DSPMem+069h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6A       ; Voice  6
      mov [DSPMem+06Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6B       ; Voice  6
      mov [DSPMem+06Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6C       ; Voice  6
      mov [DSPMem+06Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6D       ; Voice  6
      mov [DSPMem+06Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6E       ; Voice  6
      mov [DSPMem+06Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg6F       ; Voice  6  Echo coefficient
      mov [DSPMem+06Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg70       ; Voice  7  Volume Left
      mov [DSPMem+70h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg71       ; Voice  7  Volume Right
      mov [DSPMem+71h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg72       ; Voice  7  Pitch Low
      mov [DSPMem+072h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg73       ; Voice  7  Pitch High
      mov [DSPMem+073h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg74       ; Voice  7  SCRN
      mov [DSPMem+74h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg75       ; Voice  7  ADSR (1)
      mov [DSPMem+75h],al
      VoiceAdsr 7
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg76       ; Voice  7  ADSR (2)
      mov [DSPMem+76h],al
      VoiceAdsr 7
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg77       ; Voice  7  GAIN
      VoiceGain 7
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg78       ; Voice  7  ENVX
      mov [DSPMem+078h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg79       ; Voice  7  OUTX
      mov [DSPMem+079h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7A       ; Voice  7
      mov [DSPMem+07Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7B       ; Voice  7
      mov [DSPMem+07Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7C       ; ENDX
      mov byte [DSPMem+07Ch],0
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7D       ; Echo Delay
      mov [DSPMem+07Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7E       ; Voice  7
      mov [DSPMem+07Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg7F       ; Voice  7  Echo coefficient
      mov [DSPMem+07Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg80       ;
      mov [DSPMem+080h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg81       ;
      mov [DSPMem+081h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg82       ;
      mov [DSPMem+082h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg83       ;
      mov [DSPMem+083h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg84       ;
      mov [DSPMem+084h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg85       ;
      mov [DSPMem+085h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg86       ;
      mov [DSPMem+086h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg87       ;
      mov [DSPMem+087h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg88       ;
      mov [DSPMem+088h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg89       ;
      mov [DSPMem+089h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8A       ;
      mov [DSPMem+08Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8B       ;
      mov [DSPMem+08Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8C       ;
      mov [DSPMem+08Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8D       ;
      mov [DSPMem+08Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8E       ;
      mov [DSPMem+08Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg8F       ;
      mov [DSPMem+08Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg90       ;
      mov [DSPMem+090h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg91       ;
      mov [DSPMem+091h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg92       ;
      mov [DSPMem+092h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg93       ;
      mov [DSPMem+093h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg94       ;
      mov [DSPMem+094h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg95       ;
      mov [DSPMem+095h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg96       ;
      mov [DSPMem+096h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg97       ;
      mov [DSPMem+097h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg98       ;
      mov [DSPMem+098h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg99       ;
      mov [DSPMem+099h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9A       ;
      mov [DSPMem+09Ah],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9B       ;
      mov [DSPMem+09Bh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9C       ;
      mov [DSPMem+09Ch],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9D       ;
      mov [DSPMem+09Dh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9E       ;
      mov [DSPMem+09Eh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPReg9F       ;
      mov [DSPMem+09Fh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA0       ;
      mov [DSPMem+0A0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA1       ;
      mov [DSPMem+0A1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA2       ;
      mov [DSPMem+0A2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA3       ;
      mov [DSPMem+0A3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA4       ;
      mov [DSPMem+0A4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA5       ;
      mov [DSPMem+0A5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA6       ;
      mov [DSPMem+0A6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA7       ;
      mov [DSPMem+0A7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA8       ;
      mov [DSPMem+0A8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegA9       ;
      mov [DSPMem+0A9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAA       ;
      mov [DSPMem+0AAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAB       ;
      mov [DSPMem+0ABh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAC       ;
      mov [DSPMem+0ACh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAD       ;
      mov [DSPMem+0ADh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAE       ;
      mov [DSPMem+0AEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegAF       ;
      mov [DSPMem+0AFh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB0       ;
      mov [DSPMem+0B0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB1       ;
      mov [DSPMem+0B1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB2       ;
      mov [DSPMem+0B2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB3       ;
      mov [DSPMem+0B3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB4       ;
      mov [DSPMem+0B4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB5       ;
      mov [DSPMem+0B5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB6       ;
      mov [DSPMem+0B6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB7       ;
      mov [DSPMem+0B7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB8       ;
      mov [DSPMem+0B8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegB9       ;
      mov [DSPMem+0B9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBA       ;
      mov [DSPMem+0BAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBB       ;
      mov [DSPMem+0BBh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBC       ;
      mov [DSPMem+0BCh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBD       ;
      mov [DSPMem+0BDh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBE       ;
      mov [DSPMem+0BEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegBF       ;
      mov [DSPMem+0BFh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC0       ;
      mov [DSPMem+0C0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC1       ;
      mov [DSPMem+0C1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC2       ;
      mov [DSPMem+0C2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC3       ;
      mov [DSPMem+0C3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC4       ;
      mov [DSPMem+0C4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC5       ;
      mov [DSPMem+0C5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC6       ;
      mov [DSPMem+0C6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC7       ;
      mov [DSPMem+0C7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC8       ;
      mov [DSPMem+0C8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegC9       ;
      mov [DSPMem+0C9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCA       ;
      mov [DSPMem+0CAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCB       ;
      mov [DSPMem+0CBh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCC       ;
      mov [DSPMem+0CCh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCD       ;
      mov [DSPMem+0CDh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCE       ;
      mov [DSPMem+0CEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegCF       ;
      mov [DSPMem+0CFh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD0       ;
      mov [DSPMem+0D0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD1       ;
      mov [DSPMem+0D1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD2       ;
      mov [DSPMem+0D2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD3       ;
      mov [DSPMem+0D3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD4       ;
      mov [DSPMem+0D4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD5       ;
      mov [DSPMem+0D5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD6       ;
      mov [DSPMem+0D6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD7       ;
      mov [DSPMem+0D7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD8       ;
      mov [DSPMem+0D8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegD9       ;
      mov [DSPMem+0D9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDA       ;
      mov [DSPMem+0DAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDB       ;
      mov [DSPMem+0DBh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDC       ;
      mov [DSPMem+0DCh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDD       ;
      mov [DSPMem+0DDh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDE       ;
      mov [DSPMem+0DEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegDF       ;
      mov [DSPMem+0DFh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE0       ;
      mov [DSPMem+0E0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE1       ;
      mov [DSPMem+0E1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE2       ;
      mov [DSPMem+0E2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE3       ;
      mov [DSPMem+0E3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE4       ;
      mov [DSPMem+0E4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE5       ;
      mov [DSPMem+0E5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE6       ;
      mov [DSPMem+0E6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE7       ;
      mov [DSPMem+0E7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE8       ;
      mov [DSPMem+0E8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegE9       ;
      mov [DSPMem+0E9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegEA       ;
      mov [DSPMem+0EAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegEB       ;
      mov [DSPMem+0EBh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegEC       ;
      mov [DSPMem+0ECh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegED       ;
      mov [DSPMem+0EDh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegEE       ;
      mov [DSPMem+0EEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegEF       ;
      mov [DSPMem+0EFh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF0       ;
      mov [DSPMem+0F0h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF1       ;
      mov [DSPMem+0F1h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF2       ;
      mov [DSPMem+0F2h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF3       ;
      mov [DSPMem+0F3h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF4       ;
      mov [DSPMem+0F4h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF5       ;
      mov [DSPMem+0F5h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF6       ;
      mov [DSPMem+0F6h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF7       ;
      mov [DSPMem+0F7h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF8       ;
      mov [DSPMem+0F8h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegF9       ;
      mov [DSPMem+0F9h],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFA       ;
      mov [DSPMem+0FAh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFB       ;
      mov [DSPMem+0FBh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFC       ;
      mov [DSPMem+0FCh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFD       ;
      mov [DSPMem+0FDh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFE       ;
      mov [DSPMem+0FEh],al
      ret
;;; asm2c:ENDSYM

NEWSYM WDSPRegFF       ;                                        ;000E436F
      mov [DSPMem+0FFh],al
      ret
;;; asm2c:ENDSYM

