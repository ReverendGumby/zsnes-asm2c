#include "check_asm2c_h.h"

START_TEST(test_op2_add32_imm)
{
    eax = 0x7fffffff;
    op2_add(eax, 0x1);
    ck_assert_uint_eq(eax, 0x80000000);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_sub16_imm)
{
    eax = 0x0;
    op2_sub(ax, 0xffff);
    ck_assert_uint_eq(ax, 0x1);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_sub16_imm_o)
{
    eax = 0x0;
    op2_sub(ax, 0xffff);
    ck_assert_uint_eq(ax, 0x1);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_cmp8_imm)
{
    ecx = 0xFFFF00FF;
    op2_cmp(ch, 0x01);
    ck_assert_uint_eq(ecx, 0xFFFF00FF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_cmp32_imm)
{
    edx = 0x000bc5a0;
    op2_cmp(edx, 0x000cc55e);
    ck_assert_uint_eq(edx, 0x000bc5a0);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_add_flags1)
{
    al = 0x3F;
    op2_add(al, 0x40);
    ck_assert_uint_eq(al, 0x7F);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_add_flags2)
{
    al = 0x7F;
    op2_add(al, 0x40);
    ck_assert_uint_eq(al, 0xBF);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_add_flags3)
{
    al = 0x7F;
    op2_add(al, 0x80);
    ck_assert_uint_eq(al, 0xFF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_add_flags4)
{
    al = 0xFF;
    op2_add(al, 0x80);
    ck_assert_uint_eq(al, 0x7F);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_add_flags5)
{
    al = 0xFF;
    op2_add(al, 0x01);
    ck_assert_uint_eq(al, 0x00);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op2_sub_flags1)
{
    al = 0x3F;
    op2_sub(al, 0x40);
    ck_assert_uint_eq(al, 0xFF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
}
END_TEST

START_TEST(test_op2_sub_flags2)
{
    al = 0x7F;
    op2_sub(al, 0x40);
    ck_assert_uint_eq(al, 0x3F);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sub_flags3)
{
    al = 0x7F;
    op2_sub(al, 0x80);
    ck_assert_uint_eq(al, 0xFF);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sub_flags4)
{
    al = 0xFF;
    op2_sub(al, 0x80);
    ck_assert_uint_eq(al, 0x7F);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sub_flags5)
{
    al = 0xFF;
    op2_sub(al, 0xFF);
    ck_assert_uint_eq(al, 0x00);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sbb_flags1)
{
    EFLAGS.cf = 0;
    al = 0x3F;
    op2_sbb(al, 0x40);
    ck_assert_uint_eq(al, 0xff);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
}
END_TEST

START_TEST(test_op2_sbb_flags2)
{
    EFLAGS.cf = 1;
    al = 0x7F;
    op2_sbb(al, 0x40);
    ck_assert_uint_eq(al, 0x3e);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sbb_flags3)
{
    EFLAGS.cf = 0;
    al = 0x7F;
    op2_sbb(al, 0x80);
    ck_assert_uint_eq(al, 0xff);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sbb_flags4)
{
    EFLAGS.cf = 1;
    al = 0xFF;
    op2_sbb(al, 0x80);
    ck_assert_uint_eq(al, 0x7e);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op2_sbb_flags5)
{
    al = 0xFF;
    op2_sbb(al, 0xFF);
    ck_assert_uint_eq(al, 0x00);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_neg_flags1)
{
    ax = 0x0000;
    op1_neg(ax);
    ck_assert_uint_eq(ax, 0x0000);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_neg_flags2)
{
    ax = 0x7FFF;
    op1_neg(ax);
    ck_assert_uint_eq(ax, 0x8001);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_neg_flags3)
{
    ax = 0x8000;
    op1_neg(ax);
    ck_assert_uint_eq(ax, 0x8000);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_neg_flags4)
{
    ax = 0x8001;
    op1_neg(ax);
    ck_assert_uint_eq(ax, 0x7FFF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    //ck_assert(EFLAGS.af == 0);        // AF is wrong, but no one cares.
}
END_TEST

START_TEST(test_op1_neg_flags5)
{
    ax = 0xFFFF;
    op1_neg(ax);
    ck_assert_uint_eq(ax, 0x0001);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_inc_flags1)
{
    ax = 0x0000;
    op1_inc(ax);
    ck_assert_uint_eq(ax, 0x0001);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_inc_flags2)
{
    ax = 0x7FFF;
    op1_inc(ax);
    ck_assert_uint_eq(ax, 0x8000);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_inc_flags3)
{
    ax = 0x8000;
    op1_inc(ax);
    ck_assert_uint_eq(ax, 0x8001);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_inc_flags4)
{
    ax = 0xFFFF;
    op1_inc(ax);
    ck_assert_uint_eq(ax, 0x0000);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_dec_flags1)
{
    ax = 0xFFFF;
    op1_dec(ax);
    ck_assert_uint_eq(ax, 0xFFFE);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_dec_flags2)
{
    ax = 0x8000;
    op1_dec(ax);
    ck_assert_uint_eq(ax, 0x7FFF);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_dec_flags3)
{
    ax = 0x7FFF;
    op1_dec(ax);
    ck_assert_uint_eq(ax, 0x7FFE);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_dec_flags4)
{
    ax = 0x0001;
    op1_dec(ax);
    ck_assert_uint_eq(ax, 0x0000);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op1_dec_flags5)
{
    ax = 0x0000;
    op1_dec(ax);
    ck_assert_uint_eq(ax, 0xFFFF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_daa1)
{
    al = 0x94;
    op2_add(al, 0x95);
    op0_daa();
    ck_assert_uint_eq(al, 0x89);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op0_daa2)
{
    al = 0x95;
    op2_add(al, 0x95);
    op0_daa();
    ck_assert_uint_eq(al, 0x90);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_daa3)
{
    al = 0x99;
    op2_add(al, 0x99);
    op0_daa();
    ck_assert_uint_eq(al, 0x98);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_daa4)
{
    al = 0x9A;
    op2_add(al, 0x90);
    op0_daa();
    ck_assert_uint_eq(al, 0x90);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_daa5)
{
    al = 0x0F;
    op2_add(al, 0x91);
    op0_daa();
    ck_assert_uint_eq(al, 0x06);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_das1)
{
    al = 0x99;
    op2_sub(al, 0x90);
    op0_das();
    ck_assert_uint_eq(al, 0x09);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op0_das2)
{
    al = 0x99;
    op2_sub(al, 0x99);
    op0_das();
    ck_assert_uint_eq(al, 0x00);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
    ck_assert(EFLAGS.af == 0);
}
END_TEST

START_TEST(test_op0_das3)
{
    al = 0x98;
    op2_sub(al, 0x99);
    op0_das();
    ck_assert_uint_eq(al, 0x99);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_das4)
{
    al = 0x95;
    op2_sub(al, 0x0A);
    op0_das();
    ck_assert_uint_eq(al, 0x85);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op0_das5)
{
    al = 0x95;
    op2_sub(al, 0x0F);
    op0_das();
    ck_assert_uint_eq(al, 0x80);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
    ck_assert(EFLAGS.af == 1);
}
END_TEST

START_TEST(test_op1_mul_byte)
{
    al = 0x91;
    bl = 0x03;
    op1_mul_byte(bl);
    ck_assert_uint_eq(ax, 0x01b3);
}
END_TEST

START_TEST(test_op1_mul_word)
{
    ax = 0x9111;
    bx = 0x0013;
    op1_mul_word(bx);
    ck_assert_uint_eq(ax, 0xc443);
    ck_assert_uint_eq(dx, 0x000a);
}
END_TEST

START_TEST(test_op1_mul_dword)
{
    eax = 0x91111111;
    ebx = 0x0213;
    op1_mul_dword(ebx);
    ck_assert_uint_eq(eax, 0xe6666643);
    ck_assert_uint_eq(edx, 0x0000012c);
}
END_TEST

START_TEST(test_op1_imul_byte1)
{
    al = 0x03;
    bl = 0x91;
    op1_imul_byte(bl);
    ck_assert_uint_eq(ax, 0xfeb3);
}
END_TEST

START_TEST(test_op1_imul_byte2)
{
    al = 0x91;
    bl = 0x03;
    op1_imul_byte(bl);
    ck_assert_uint_eq(ax, 0xfeb3);
}
END_TEST

START_TEST(test_op1_imul_word1)
{
    ax = 0x0013;
    bx = 0x9111;
    op1_imul_word(bx);
    ck_assert_uint_eq(ax, 0xc443);
    ck_assert_uint_eq(dx, 0xfff7);
}
END_TEST

START_TEST(test_op1_imul_word2)
{
    ax = 0x9111;
    bx = 0x0013;
    op1_imul_word(bx);
    ck_assert_uint_eq(ax, 0xc443);
    ck_assert_uint_eq(dx, 0xfff7);
}
END_TEST

START_TEST(test_op1_imul_dword1)
{
    eax = 0x00000213;
    ebx = 0x91111111;
    op1_imul_dword(ebx);
    ck_assert_uint_eq(eax, 0xe6666643);
    ck_assert_uint_eq(edx, 0xffffff19);
}
END_TEST

START_TEST(test_op1_imul_dword2)
{
    eax = 0x91111111;
    ebx = 0x00000213;
    op1_imul_dword(ebx);
    ck_assert_uint_eq(eax, 0xe6666643);
    ck_assert_uint_eq(edx, 0xffffff19);
}
END_TEST

START_TEST(test_op2_imul_byte)
{
    al = 0x03;
    bl = 0x91;
    op2_imul_byte(al, bl);
    ck_assert_uint_eq(al, 0xb3);
}
END_TEST

START_TEST(test_op2_imul_word)
{
    ax = 0x0013;
    bx = 0x9111;
    op2_imul_word(ax, bx);
    ck_assert_uint_eq(ax, 0xc443);
}
END_TEST

START_TEST(test_op2_imul_dword)
{
    eax = 0x00000213;
    ebx = 0x91111111;
    op2_imul_dword(eax, ebx);
    ck_assert_uint_eq(eax, 0xe6666643);
}
END_TEST

START_TEST(test_op1_div_word)
{
    ax = 0xf682;
    cx = 0x002d;
    op1_div_word(cx);
    ck_assert_uint_eq(ax, 0x057a);
    ck_assert_uint_eq(dx, 0x0010);
}
END_TEST

START_TEST(test_op1_div_dword)
{
    eax = 0xc671ea84;
    ebx = 0x10038bce;
    op1_div_dword(ebx);
    ck_assert_uint_eq(eax, 0x0000000c);
    ck_assert_uint_eq(edx, 0x06475cdc);
}
END_TEST

TCase* tc_math(void)
{
    TCase* tc = tcase_create("math");

    tcase_add_checked_fixture(tc, setup, teardown);
    tcase_add_test(tc, test_op2_add32_imm);
    tcase_add_test(tc, test_op2_sub16_imm);
    tcase_add_test(tc, test_op2_cmp8_imm);
    tcase_add_test(tc, test_op2_cmp32_imm);

    tcase_add_test(tc, test_op2_add_flags1);
    tcase_add_test(tc, test_op2_add_flags2);
    tcase_add_test(tc, test_op2_add_flags3);
    tcase_add_test(tc, test_op2_add_flags4);
    tcase_add_test(tc, test_op2_add_flags5);

    tcase_add_test(tc, test_op2_sub_flags1);
    tcase_add_test(tc, test_op2_sub_flags2);
    tcase_add_test(tc, test_op2_sub_flags3);
    tcase_add_test(tc, test_op2_sub_flags4);
    tcase_add_test(tc, test_op2_sub_flags5);

    tcase_add_test(tc, test_op2_sbb_flags1);
    tcase_add_test(tc, test_op2_sbb_flags2);
    tcase_add_test(tc, test_op2_sbb_flags3);
    tcase_add_test(tc, test_op2_sbb_flags4);
    tcase_add_test(tc, test_op2_sbb_flags5);

    tcase_add_test(tc, test_op1_neg_flags1);
    tcase_add_test(tc, test_op1_neg_flags2);
    tcase_add_test(tc, test_op1_neg_flags3);
    tcase_add_test(tc, test_op1_neg_flags4);
    tcase_add_test(tc, test_op1_neg_flags5);

    tcase_add_test(tc, test_op1_inc_flags1);
    tcase_add_test(tc, test_op1_inc_flags2);
    tcase_add_test(tc, test_op1_inc_flags3);
    tcase_add_test(tc, test_op1_inc_flags4);

    tcase_add_test(tc, test_op1_dec_flags1);
    tcase_add_test(tc, test_op1_dec_flags2);
    tcase_add_test(tc, test_op1_dec_flags3);
    tcase_add_test(tc, test_op1_dec_flags4);
    tcase_add_test(tc, test_op1_dec_flags5);

    tcase_add_test(tc, test_op0_daa1);
    tcase_add_test(tc, test_op0_daa2);
    tcase_add_test(tc, test_op0_daa3);
    tcase_add_test(tc, test_op0_daa4);
    tcase_add_test(tc, test_op0_daa5);

    tcase_add_test(tc, test_op0_das1);
    tcase_add_test(tc, test_op0_das2);
    tcase_add_test(tc, test_op0_das3);
    tcase_add_test(tc, test_op0_das4);
    tcase_add_test(tc, test_op0_das5);

    tcase_add_test(tc, test_op1_mul_byte);
    tcase_add_test(tc, test_op1_mul_word);
    tcase_add_test(tc, test_op1_mul_dword);

    tcase_add_test(tc, test_op1_imul_byte1);
    tcase_add_test(tc, test_op1_imul_byte2);
    tcase_add_test(tc, test_op1_imul_word1);
    tcase_add_test(tc, test_op1_imul_word2);
    tcase_add_test(tc, test_op1_imul_dword1);
    tcase_add_test(tc, test_op1_imul_dword2);

    tcase_add_test(tc, test_op2_imul_byte);
    tcase_add_test(tc, test_op2_imul_word);
    tcase_add_test(tc, test_op2_imul_dword);

    tcase_add_test(tc, test_op1_div_word);
    tcase_add_test(tc, test_op1_div_dword);

    return tc;
}
