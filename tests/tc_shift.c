#include "check_asm2c_h.h"

START_TEST(test_op2_shl_one)
{
    eax = 0x20000000U;
    op2_shl(eax, 1);
    ck_assert_uint_eq(eax, 0x40000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);

    op2_shl(eax, 1);
    ck_assert_uint_eq(eax, 0x80000000U);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);

    op2_shl(eax, 1);
    ck_assert_uint_eq(eax, 0x00000000U);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

START_TEST(test_op2_shl_lots)
{
    eax = 0x00000001U;
    op2_shl(eax, 31);
    ck_assert_uint_eq(eax, 0x80000000U);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);

    eax = 0x10000000U;
    op2_shl(eax, 4);
    ck_assert_uint_eq(eax, 0x00000000U);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

START_TEST(test_op2_shr_one)
{
    eax = 0x80000000U;
    op2_shr(eax, 1);
    ck_assert_uint_eq(eax, 0x40000000U);
    ck_assert(EFLAGS.of == 1);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);

    op2_shr(eax, 1);
    ck_assert_uint_eq(eax, 0x20000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);

    eax = 0x00000001U;
    op2_shr(eax, 1);
    ck_assert_uint_eq(eax, 0x00000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

START_TEST(test_op2_shr_lots)
{
    eax = 0x80000000U;
    op2_shr(eax, 31);
    ck_assert_uint_eq(eax, 0x00000001U);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);

    eax = 0x00000008U;
    op2_shr(eax, 4);
    ck_assert_uint_eq(eax, 0x00000000U);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

START_TEST(test_op2_sar_one)
{
    eax = 0x80000000U;
    op2_sar(eax, 1);
    ck_assert_uint_eq(eax, 0xC0000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);

    op2_sar(eax, 1);
    ck_assert_uint_eq(eax, 0xE0000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);

    eax = 0x7FFFFFFFU;
    op2_sar(eax, 1);
    ck_assert_uint_eq(eax, 0x3FFFFFFFU);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
}
END_TEST

START_TEST(test_op2_sar_lots)
{
    eax = 0x80000000U;
    op2_sar(eax, 15);
    ck_assert_uint_eq(eax, 0xFFFF0000U);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);

    eax = 0x7FFFFFFFU;
    op2_sar(eax, 31);
    ck_assert_uint_eq(eax, 0x00000000U);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 1);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

TCase* tc_shift(void)
{
    TCase* tc = tcase_create("shift");

    tcase_add_checked_fixture(tc, setup, teardown);
    tcase_add_test(tc, test_op2_shl_one);
    tcase_add_test(tc, test_op2_shl_lots);
    tcase_add_test(tc, test_op2_shr_one);
    tcase_add_test(tc, test_op2_shr_lots);
    tcase_add_test(tc, test_op2_sar_one);
    tcase_add_test(tc, test_op2_sar_lots);

    return tc;
}
