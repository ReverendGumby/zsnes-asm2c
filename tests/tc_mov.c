#include "check_asm2c_h.h"

START_TEST(test_op2_mov32_imm)
{
    op2_mov(eax, 0x12345678);
    ck_assert_uint_eq(eax, 0x12345678);
}
END_TEST

START_TEST(test_op2_mov16_imm)
{
    op2_mov(ebx, 0x12340000);
    op2_mov(bx, 0xfdce);
    ck_assert_uint_eq(bx, 0xfdce);
    ck_assert_uint_eq(ebx, 0x1234fdce);
}
END_TEST

START_TEST(test_op2_mov8_imm)
{
    op2_mov(cl, 0x34);
    op2_mov(ch, 0x12);
    ck_assert_uint_eq(cl, 0x34);
    ck_assert_uint_eq(ch, 0x12);
    ck_assert_uint_eq(cx, 0x1234);
    ck_assert_uint_eq(ecx, 0x00001234);
}
END_TEST

START_TEST(test_op2_mov32_reg2reg)
{
    op2_mov(eax, 0x12345678);
    op2_mov(ebx, eax);
    ck_assert_uint_eq(ebx, 0x12345678);
}
END_TEST

START_TEST(test_op2_mov16_reg2reg)
{
    op2_mov(eax, 0x12345678);
    op2_mov(si, ax);
    ck_assert_uint_eq(si, 0x5678);
}
END_TEST

START_TEST(test_op2_mov8_reg2reg)
{
    op2_mov(eax, 0x12345678);
    op2_mov(dh, ah);
    ck_assert_uint_eq(dh, 0x56);
}
END_TEST

START_TEST(test_op2_mov32_reg2mem)
{
    op2_mov(eax, 0x12345678);
    op2_mov(m_dword(0x100), eax);

    ck_assert_uint_eq(_ram[0x0ff], MEM_INIT);
    ck_assert_uint_eq(_ram[0x100], 0x78);
    ck_assert_uint_eq(_ram[0x101], 0x56);
    ck_assert_uint_eq(_ram[0x102], 0x34);
    ck_assert_uint_eq(_ram[0x103], 0x12);
    ck_assert_uint_eq(_ram[0x104], MEM_INIT);
}
END_TEST

START_TEST(test_op2_mov16_reg2mem)
{
    op2_mov(ecx, 0x12345678);
    op2_mov(m_word(0x911), cx);

    ck_assert_uint_eq(_ram[0x910], MEM_INIT);
    ck_assert_uint_eq(_ram[0x911], 0x78);
    ck_assert_uint_eq(_ram[0x912], 0x56);
    ck_assert_uint_eq(_ram[0x913], MEM_INIT);
}
END_TEST

START_TEST(test_op2_mov8_reg2mem)
{
    op2_mov(ebx, 0x12345678);
    op2_mov(m_byte(0x503), bh);

    ck_assert_uint_eq(_ram[0x500], MEM_INIT);
    ck_assert_uint_eq(_ram[0x501], MEM_INIT);
    ck_assert_uint_eq(_ram[0x502], MEM_INIT);
    ck_assert_uint_eq(_ram[0x503], 0x56);
    ck_assert_uint_eq(_ram[0x504], MEM_INIT);
    ck_assert_uint_eq(_ram[0x505], MEM_INIT);
    ck_assert_uint_eq(_ram[0x506], MEM_INIT);
    ck_assert_uint_eq(_ram[0x507], MEM_INIT);
}
END_TEST

START_TEST(test_op2_mov32_mem2reg)
{
    m_dword(0x100) = 0x12345678;
    op2_mov(eax, m_dword(0x100));
    ck_assert_uint_eq(eax, 0x12345678);
}
END_TEST

START_TEST(test_op2_mov16_mem2reg)
{
    m_dword(0x910) = 0x12345678;
    op2_mov(ax, m_word(0x911));
    ck_assert_uint_eq(ax, 0x3456);
}
END_TEST

START_TEST(test_op2_mov8_mem2reg)
{
    m_dword(0x500) = 0x12345678;
    op2_mov(dl, 0xfe);
    op2_mov(dh, m_byte(0x503));
    ck_assert_uint_eq(dx, 0x12fe);
}
END_TEST

TCase* tc_mov(void)
{
    TCase* tc = tcase_create("mov");

    tcase_add_checked_fixture(tc, setup, teardown);
    tcase_add_test(tc, test_op2_mov32_imm);
    tcase_add_test(tc, test_op2_mov16_imm);
    tcase_add_test(tc, test_op2_mov8_imm);
    tcase_add_test(tc, test_op2_mov32_reg2reg);
    tcase_add_test(tc, test_op2_mov16_reg2reg);
    tcase_add_test(tc, test_op2_mov8_reg2reg);
    tcase_add_test(tc, test_op2_mov32_reg2mem);
    tcase_add_test(tc, test_op2_mov16_reg2mem);
    tcase_add_test(tc, test_op2_mov8_reg2mem);
    tcase_add_test(tc, test_op2_mov32_mem2reg);
    tcase_add_test(tc, test_op2_mov16_mem2reg);
    tcase_add_test(tc, test_op2_mov8_mem2reg);

    return tc;
}
