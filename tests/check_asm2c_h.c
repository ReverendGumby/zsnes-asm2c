#include "check_asm2c_h.h"

uint8_t* _ram;
state_t _st;

void setup(void)
{
    memset(&_st, 0, sizeof(_st));
    memset(_ram, MEM_INIT, MEM_SIZE);
}

void teardown(void)
{
}

//////////////////////////////////////////////////////////////////////

Suite* asm2c_h_suite(void)
{
    Suite* s;
    extern TCase* tc_mov(void);
    extern TCase* tc_logic(void);
    extern TCase* tc_math(void);
    extern TCase* tc_shift(void);

    s = suite_create("asm2c_h");
    suite_add_tcase(s, tc_mov());               /* MOV */
    suite_add_tcase(s, tc_logic());             /* AND, OR, XOR */
    suite_add_tcase(s, tc_math());              /* ADD, SUB, CMP */
    suite_add_tcase(s, tc_shift());             /* SHL, ROL, ... */

    return s;
}

int main(void)
{
    int number_failed;
    Suite* s;
    SRunner* sr;

    _ram = malloc(MEM_SIZE);

    s = asm2c_h_suite();
    sr = srunner_create(s);

    srunner_run_all(sr, CK_NORMAL);
    number_failed = srunner_ntests_failed(sr);
    srunner_free(sr);

    free(_ram);

    return (number_failed == 0) ? EXIT_SUCCESS : EXIT_FAILURE;
}
