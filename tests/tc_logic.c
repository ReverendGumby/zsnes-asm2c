#include "check_asm2c_h.h"

START_TEST(test_op2_and32_imm)
{
    eax = 0x12345678;
    EFLAGS.of = 1;
    EFLAGS.cf = 1;
    EFLAGS.sf = 1;
    EFLAGS.zf = 1;
    op2_and(eax, 0x55555555);
    ck_assert_uint_eq(eax, 0x10145450);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 0);
}
END_TEST

START_TEST(test_op2_or16_imm)
{
    eax = 0x12345678;
    EFLAGS.of = 1;
    EFLAGS.cf = 1;
    EFLAGS.sf = 0;
    EFLAGS.zf = 1;
    op2_or(ax, (uint32_t)~0x5678);
    ck_assert_uint_eq(eax, 0x1234FFFF);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 1);
    ck_assert(EFLAGS.zf == 0);
}
END_TEST

START_TEST(test_op2_xor8_imm)
{
    eax = 0x12345678;
    EFLAGS.of = 1;
    EFLAGS.cf = 1;
    EFLAGS.sf = 1;
    EFLAGS.zf = 0;
    op2_xor(al, (uint32_t)0x78);
    ck_assert_uint_eq(eax, 0x12345600);
    ck_assert(EFLAGS.of == 0);
    ck_assert(EFLAGS.cf == 0);
    ck_assert(EFLAGS.sf == 0);
    ck_assert(EFLAGS.zf == 1);
}
END_TEST

TCase* tc_logic(void)
{
    TCase* tc = tcase_create("logic");

    tcase_add_checked_fixture(tc, setup, teardown);
    tcase_add_test(tc, test_op2_and32_imm);
    tcase_add_test(tc, test_op2_or16_imm);
    tcase_add_test(tc, test_op2_xor8_imm);

    return tc;
}
