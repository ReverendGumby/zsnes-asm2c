;Copyright (C) 1997-2001 ZSNES Team ( zsknight@zsnes.com / _demo_@zsnes.com )
;Copyright (C) 2023 David Hunter
;
;This file is part of the zsnes-asm2c project.
;
;This program is free software; you can redistribute it and/or
;modify it under the terms of the GNU General Public License
;as published by the Free Software Foundation; either
;version 2 of the License, or (at your option) any later
;version.
;
;This program is distributed in the hope that it will be useful,
;but WITHOUT ANY WARRANTY; without even the implied warranty of
;MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;GNU General Public License for more details.
;
;You should have received a copy of the GNU General Public License
;along with this program; if not, write to the Free Software
;Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


//////////////////////////////////////////////////////////////////////

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; From cpu/spcdef.inc

; move al at address ebx

; branch instructions
%macro spcbrancher 0
      inc ebp
      ret
.branch
      movsx ebx,byte [ebp]
      inc ebp
      add ebp,ebx
      ret
%endmacro      

; tcall instruction
%macro spctcall 1
      mov ebx,ebp
      sub ebx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],bh
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],bl
      dec byte [spcS]
      mov bx,[spcextraram+%1]
      add ebx,spcRam
      mov ebp,ebx
      ret
%endmacro

; SET1 instruction
%macro set1 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      or al,%1
      WriteByte
      ret
%endmacro

; CLR1 instruction
%macro clr1 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      and al,%1
      WriteByte
      ret
%endmacro

; BBS instruction
%macro bbs 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      test byte[ebx],%1
      jnz .dp0jump
      add ebp,2
      ret
.dp0jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      ret
%endmacro

; BBC instruction
%macro bbc 1
      mov bl,[ebp]
      add ebx,[spcRamDP]
      test byte[ebx],%1
      jz .dp0jump
      add ebp,2
      ret
.dp0jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      ret
%endmacro

; OR A, instruction
%macro SPC_OR_A 1
      or byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      ret
%endmacro

; AND A, instruction
%macro SPC_AND_A 1
      and byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      ret
%endmacro

; EOR A, instruction
%macro SPC_EOR_A 1
      xor byte [spcA], %1
      mov al,[spcA]
      mov [spcNZ],al
      ret
%endmacro

; CMP A, instruction
%macro SPC_CMP_A 1
      cmp byte [spcA], %1
      cmc
      SPCSetFlagnzc
%endmacro

%macro SPC_ADC_A 1
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc byte [spcA], %1
      SPCSetFlagnvhzc
%endmacro

%macro SPC_SBC_A 1
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb byte [spcA], %1
      cmc
      SPCSetFlagnvhzc
%endmacro

%macro SPC_MOV_A 0
      mov byte [spcA], al
      mov [spcNZ],al
      ret
%endmacro


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; From cpu/spcaddr.inc

; A,DP addressing mode
%macro SPCaddr_DP 0
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
%endmacro

%macro SPCaddr_DP_X 0
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
%endmacro

%macro SPCaddr_LABS 0
      mov bx,[ebp]
      add ebp,2
      mov al,[spcRam+ebx]
%endmacro

%macro SPCaddr_LABS_X 1
      mov %1,[spcX]
      add bx,[ebp]
      add ebp,2
      mov al,[spcRam+ebx]
%endmacro

; A,(X)
%macro SPCaddr__X_ 0
      mov bl,[spcX]
      add ebx,[spcRamDP]
      mov al,[ebx]
%endmacro

; A,labs+Y
%macro SPCaddr_LABS_Y 0
      mov bl,[spcY]
      add bx,[ebp]
      add ebp,2
      mov al,[spcRam+ebx]
%endmacro

; A,(DP,X)
%macro SPCaddr_bDP_Xb 0
      mov bl,[ebp]
      add bl,[spcX]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      mov bl,[spcRam+eax]
%endmacro

%macro SPCaddr_bDPb_Y 0
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      add ax,[spcY]
      mov bl,[spcRam+eax]
%endmacro


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; From cpu/spc700.asm

;;; asm2c:EXTERN spcRptr void (*spcRptr[0x10])(void)
;;; asm2c:EXTERN spcWptr void (*spcWptr[0x10])(void)
;;; asm2c:EXTERN dspRptr void (*dspRptr[0x100])(void)
;;; asm2c:EXTERN dspWptr void (*dspWptr[0x100])(void)

%macro WriteByte 0
  cmp ebx,0ffh+spcRam
  ja %%extramem
  cmp ebx,0f0h+spcRam
  jb %%normalmem
  sub ebx,spcRam
;  call dword near [spcWptr+ebx*4-0f0h*4]
;;; asm2c:CLINE spcWptr[ebx - 0xf0]();
  jmp %%finished
%%extramem
  cmp ebx,0ffc0h+spcRam
  jb %%normalmem
  sub ebx,spcRam
  sub ebx,0ffc0h
  mov [spcextraram+ebx],al
  jmp near %%finished
%%normalmem
  mov [ebx],al
%%finished
%endmacro

%macro ReadByte 0
  cmp ebx,0ffh+spcRam
  ja .rnormalmem
  cmp ebx,0f0h+spcRam
  jb .rnormalmem2
  sub ebx,spcRam
;  call dword near [spcRptr+ebx*4-0f0h*4]
;;; asm2c:CLINE spcRptr[ebx - 0xf0]();
  jmp .rfinished
.rnormalmem
.rnormalmem2
   mov al,[ebx]
.rfinished
%endmacro

; This function is called every scanline (262*60 times/sec)
; Make it call 0.9825 times (393/400) (skip when divisible by 64)
; 2 8khz, 1 64khz

NEWSYM updatetimer                                              ;000DD387
.another
      xor byte[timrcall],01h
      test byte[timrcall],01h
      jz short .notimer
      test byte[timeron],1
      jz short .noin0
      dec byte[timinl0]
      jnz short .noin0
      inc byte[spcRam+0FDh]
      mov al,[timincr0]
      mov [timinl0],al
.noin0
      test byte[timeron],2
      jz short .noin1
      dec byte[timinl1]
      jnz short .noin1
      inc byte[spcRam+0FEh]
      mov al,[timincr1]
      mov [timinl1],al
.noin1
.notimer
      test byte[timeron],4
      jz short .noin2d2
      dec byte[timinl2]
      jnz short .noin2
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
.noin2
      dec byte[timinl2]
      jnz short .noin2b
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
.noin2b
      dec byte[timinl2]
      jnz short .noin2c
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
.noin2c
      dec byte[timinl2]
      jnz short .noin2d
      inc byte[spcRam+0FFh]
      mov al,[timincr2]
      mov [timinl2],al
.noin2d
.noin2d2


      ret
;;; asm2c:ENDSYM


; SPC Write Registers
; DO NOT MODIFY DX OR ECX!
NEWSYM SPCRegF0                 ;000DD443
      mov [spcRam+0F0h],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF1                 ;000DD449
      test al,10h
      jz .No01Clear
      mov byte [spcRam+0F4h],0
      mov byte [spcRam+0F5h],0
.No01Clear
      test al,20h
      jz .No23Clear
      mov byte [spcRam+0F6h],0
      mov byte [spcRam+0F7h],0
.No23Clear
      and al,0Fh
      mov [spcRam+ebx],al
      mov [timeron],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF2                 ;000DD47B
      mov [spcRam+0F2h],al
      push eax
      push ebx
      xor eax,eax
      mov al,[spcRam+0F2h]
      mov bl,[DSPMem+eax]
      mov [spcRam+0F3h],bl
      pop ebx
      pop eax
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF3                 ;000DD498
      push ebx
      xor ebx,ebx
      mov bl,[spcRam+0F2h]
      and bl,07fh
;      call dword near [dspWptr+ebx*4]
;;; asm2c:CLINE dspWptr[ebx]();
      pop ebx
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF4                 ;000DD4B3
      mov [reg1read],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF5                 ;000DD4B9
      mov [reg2read],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF6                 ;000DD4BF
      mov [reg3read],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF7                 ;000DD4C5
      mov [reg4read],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF8                 ;000DD4CB
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegF9                 ;000DD4D2
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFA                 ;000DD4D9
      mov [timincr0],al
      mov [timinl0],al
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFB                 ;000DD4EA
      mov [timincr1],al
      mov [timinl1],al
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFC                 ;000DD4FB
      mov [timincr2],al
      mov [timinl2],al
      mov [spcRam+ebx],al
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFD                 ;000DD50C
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFE                 ;000DD50D
      ret
;;; asm2c:ENDSYM
NEWSYM SPCRegFF                 ;000DD50E
      ret
;;; asm2c:ENDSYM

; SPC Read Registers
; DO NOT MODIFY ANY REG!
; return data true al
NEWSYM RSPCRegF0                ;000DD50F
      mov al,[spcRam+0f0h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF1                ;000DD515
      mov al,[spcRam+0f1h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF2                ;000DD51B
      mov al,[spcRam+0f2h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF3                ;000DD521
      mov al,[spcRam+0f3h]
      push ebx
      xor ebx,ebx
      mov bl,[spcRam+0f2h]
;      call [dspRptr+ebx*4]
;;; asm2c:CLINE dspRptr[ebx]();
      pop ebx
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF4                ;000DD538
      mov al,[spcRam+0f4h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF5                ;000DD53E
      mov al,[spcRam+0f5h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF6                ;000DD544
      mov al,[spcRam+0f6h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF7                ;000DD54A
      mov al,[spcRam+0f7h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF8                ;000DD550
      mov al,[spcRam+0f8h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegF9                ;000DD556
      mov al,[spcRam+0f9h]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFA                ;000DD55C
      mov al,[spcRam+0fah]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFB                ;000DD562
      mov al,[spcRam+0fbh]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFC                ;000DD568
      mov al,[spcRam+0fch]
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFD                ;000DD56E
      mov al,[spcRam+0fdh]
      mov byte [spcRam+0fdh],0
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFE                ;000DD57B
      mov al,[spcRam+0feh]
      mov byte [spcRam+0fdh],0
      ret
;;; asm2c:ENDSYM
NEWSYM RSPCRegFF                ;000DD588
      mov al,[spcRam+0ffh]
      mov byte [spcRam+0fdh],0
      ret
;;; asm2c:ENDSYM

;************************************************

%macro SPCSetFlagnzc 0
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setcarryflag
  and byte [spcP],0FEh
  ret
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setcarryflag
  and byte [spcP],0FEh
  ret
.setzeroflag
  mov byte [spcNZ],0
  jc .setcarryflag
  and byte [spcP],0FEh
  ret
.setcarryflag
  or byte [spcP],1
  ret
%endmacro

%macro SPCSetFlagnzcnoret 0
  js short .setsignflag
  jz short .setzeroflag
  mov byte [spcNZ],1
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setsignflag
  mov byte [spcNZ],80h
  jz short .setzeroflag
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc short .setcarryflag
  and byte [spcP],0FEh
  jmp .skipflags
.setcarryflag
  or byte [spcP],1
.skipflags
%endmacro

%macro SPCSetFlagnvhzc 0
  lahf
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags 
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setoverflowflag
  or byte [spcP],01h
  jmp .skipflags
.skipflags
  and byte [spcP],0F6h
  test ah,01h
  jz .noCarry
  or byte [spcP],1
  test ah,10h
  jz .nohf
  or byte [spcP],8
.noCarry
  test ah,10h
  jz .nohf
  or byte [spcP],8
.nohf
 ret
%endmacro

%macro SPCSetFlagnvhzcnoret 0
  lahf
  js .setsignflag
  jz .setzeroflag
  mov byte [spcNZ],1
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags 
.setsignflag
  mov byte [spcNZ],80h
  jz .setzeroflag
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setzeroflag
  mov byte [spcNZ],0
  jc .setoverflowflag
  and byte [spcP],0FEh
  jmp .skipflags
.setoverflowflag
  or byte [spcP],01h
  jmp .skipflags
.skipflags
  and byte [spcP],0F6h
  test ah,01h
  jz .noCarry
  or byte [spcP],1
  test ah,10h
  jz .nohf
  or byte [spcP],8
.noCarry
  test ah,10h
  jz .nohf
  or byte [spcP],8
.nohf
%endmacro

%macro spcgetdp_imm 0
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      mov ah,[ebp]
      mov al,[ebx]
      add ebp,2
%endmacro

%macro spcaddrDPbDb_DPbSb 0
      xor ecx,ecx
      mov cl,[ebp]
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      add ebp,2
      add ecx,[spcRamDP]
      mov al,[ebx]
      mov ah,[ecx]
%endmacro

%macro spcaddrmembit 0
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
%endmacro

%macro spcROLstuff 0
      rcl al,1
      jc %%setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp %%skipflags
%%setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
%%skipflags
%endmacro

%macro spcRORstuff 0
      rcr al,1
      jc %%setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      jmp %%skipflags
%%setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
%%skipflags
%endmacro

NEWSYM Op00       ; NOP
      ret
;;; asm2c:ENDSYM
NEWSYM Op10       ; BPL Branch on N=0
      test byte [spcNZ],128
      jz .branch
      spcbrancher
;  CLRP               20    1     2   clear direct page flag    ..0..... 
;;; asm2c:ENDSYM
NEWSYM Op20       ; CLRP Clear direct page flag
      and byte [spcP],11011111b
      mov dword [spcRamDP],spcRam
      ret
;;; asm2c:ENDSYM
NEWSYM Op30       ; BMI Branch on N=1
      test byte [spcNZ],128
      jnz .branch
      spcbrancher
;  SETP               40    1     2   set dorect page flag      ..1..0..
;;; asm2c:ENDSYM
NEWSYM Op40       ; SETP Set Direct Page Flag  (Also clear interupt flag?)
      or byte [spcP],00100000b
      and byte [spcP],11111011b
      mov dword [spcRamDP],spcRam
      add dword [spcRamDP],100h
      ret
;;; asm2c:ENDSYM
NEWSYM Op50       ; BVC Branch on V=0
      test byte [spcP],64
      jz .branch
      spcbrancher
;  CLRC               60    1     2   clear carry flag          .......0 
;;; asm2c:ENDSYM
NEWSYM Op60       ; CLRC Clear carry flag
      and byte [spcP],11111110b
      ret
;;; asm2c:ENDSYM
NEWSYM Op70       ; BVS Branch on V=1
      test byte [spcP],64
      jnz .branch
      spcbrancher
;  SETC               80    1     2   set carry flag            .......1 
;;; asm2c:ENDSYM
NEWSYM Op80       ; SETC Set carry flag
      or byte [spcP],00000001b
      ret
;;; asm2c:ENDSYM
NEWSYM Op90       ; BCC Branc on c=0
      test byte [spcP],1
      jz .branch
      spcbrancher
;  EI                 A0    1     3  set interrup enable flag   .....1.. 
;;; asm2c:ENDSYM
NEWSYM OpA0       ; EI set interrupt flag
      or byte [spcP],00000100b
      ret
;;; asm2c:ENDSYM
NEWSYM OpB0       ; BCS Branch on C=1
      test byte [spcP],1
      jnz .branch
      spcbrancher
;  DI                 C0    1     3  clear interrup enable flag .....0..
;;; asm2c:ENDSYM
NEWSYM OpC0       ; DI clear interrupt flag
      and byte [spcP],11111011b
      ret
;;; asm2c:ENDSYM
NEWSYM OpD0       ; BNE branch on Z=0
      test byte [spcNZ],255
      jnz .branch
      spcbrancher
;  CLRV               E0    1     2   clear V and H             .0..0... 
;;; asm2c:ENDSYM
NEWSYM OpE0       ; CLRV clear V and H
      and byte [spcP],10111111b
      and byte [spcP],11110111b
      ret
;;; asm2c:ENDSYM
NEWSYM OpF0       ; BEQ Branch on Z=1
      test byte [spcNZ],0FFh
      jz .branch
      spcbrancher
;;; asm2c:ENDSYM
NEWSYM Op01       ; TCALL 0
      spctcall 30
;;; asm2c:ENDSYM
NEWSYM Op11       ; TCALL 1
      spctcall 28
;;; asm2c:ENDSYM
NEWSYM Op21       ; TCALL 2
      spctcall 26
;;; asm2c:ENDSYM
NEWSYM Op31       ; TCALL 3
      spctcall 24
;;; asm2c:ENDSYM
NEWSYM Op41       ; TCALL 4
      spctcall 22
;;; asm2c:ENDSYM
NEWSYM Op51       ; TCALL 5
      spctcall 20
;;; asm2c:ENDSYM
NEWSYM Op61       ; TCALL 6
      spctcall 18
;;; asm2c:ENDSYM
NEWSYM Op71       ; TCALL 7
      spctcall 16
;;; asm2c:ENDSYM
NEWSYM Op81       ; TCALL 8
      spctcall 14
;;; asm2c:ENDSYM
NEWSYM Op91       ; TCALL 9
      spctcall 12
;;; asm2c:ENDSYM
NEWSYM OpA1       ; TCALL A
      spctcall 10
;;; asm2c:ENDSYM
NEWSYM OpB1       ; TCALL B
      spctcall 08
;;; asm2c:ENDSYM
NEWSYM OpC1       ; TCALL C
      spctcall 06
;;; asm2c:ENDSYM
NEWSYM OpD1       ; TCALL D
      spctcall 04
;;; asm2c:ENDSYM
NEWSYM OpE1       ; TCALL E
      spctcall 02
;;; asm2c:ENDSYM
NEWSYM OpF1       ; TCALL F
      spctcall 00
;;; asm2c:ENDSYM
NEWSYM Op02       ; SET1 direct page bit 0
      set1 1 
;;; asm2c:ENDSYM
NEWSYM Op22       ; SET1 direct page bit 1
      set1 2
;;; asm2c:ENDSYM
NEWSYM Op42       ; SET1 direct page bit 2
      set1 4
;;; asm2c:ENDSYM
NEWSYM Op62       ; SET1 direct page bit 3
      set1 8
;;; asm2c:ENDSYM
NEWSYM Op82       ; SET1 direct page bit 4
      set1 16
;;; asm2c:ENDSYM
NEWSYM OpA2       ; SET1 direct page bit 5
      set1 32
;;; asm2c:ENDSYM
NEWSYM OpC2       ; SET1 direct page bit 6
      set1 64
;;; asm2c:ENDSYM
NEWSYM OpE2       ; SET1 direct page bit 7
      set1 128
;;; asm2c:ENDSYM
NEWSYM Op12       ; CLR1 direct page bit 0
      clr1 255-1
;;; asm2c:ENDSYM
NEWSYM Op32       ; CLR1 direct page bit 1
      clr1 255-2
;;; asm2c:ENDSYM
NEWSYM Op52       ; CLR1 direct page bit 2
      clr1 255-4
;;; asm2c:ENDSYM
NEWSYM Op72       ; CLR1 direct page bit 3
      clr1 255-8
;;; asm2c:ENDSYM
NEWSYM Op92       ; CLR1 direct page bit 4
      clr1 255-16
;;; asm2c:ENDSYM
NEWSYM OpB2       ; CLR1 direct page bit 5
      clr1 255-32
;;; asm2c:ENDSYM
NEWSYM OpD2       ; CLR1 direct page bit 6
      clr1 255-64
;;; asm2c:ENDSYM
NEWSYM OpF2       ; CLR1 direct page bit 7
      clr1 255-128
;;; asm2c:ENDSYM
NEWSYM Op03       ; BBS direct page bit 0
      bbs 1
;;; asm2c:ENDSYM
NEWSYM Op23       ; BBS direct page bit 1
      bbs 2
;;; asm2c:ENDSYM
NEWSYM Op43       ; BBS direct page bit 2
      bbs 4
;;; asm2c:ENDSYM
NEWSYM Op63       ; BBS direct page bit 3
      bbs 8
;;; asm2c:ENDSYM
NEWSYM Op83       ; BBS direct page bit 4
      bbs 16
;;; asm2c:ENDSYM
NEWSYM OpA3       ; BBS direct page bit 5
      bbs 32
;;; asm2c:ENDSYM
NEWSYM OpC3       ; BBS direct page bit 6
      bbs 64
;;; asm2c:ENDSYM
NEWSYM OpE3       ; BBS direct page bit 7
      bbs 128
;;; asm2c:ENDSYM
NEWSYM Op13       ; BBC direct page bit 0
      bbc 1
;;; asm2c:ENDSYM
NEWSYM Op33       ; BBC direct page bit 1
      bbc 2
;;; asm2c:ENDSYM
NEWSYM Op53       ; BBC direct page bit 2
      bbc 4
;;; asm2c:ENDSYM
NEWSYM Op73       ; BBC direct page bit 3
      bbc 8
;;; asm2c:ENDSYM
NEWSYM Op93       ; BBC direct page bit 4
      bbc 16
;;; asm2c:ENDSYM
NEWSYM OpB3       ; BBC direct page bit 5
      bbc 32
;;; asm2c:ENDSYM
NEWSYM OpD3       ; BBC direct page bit 6
      bbc 64
;;; asm2c:ENDSYM
NEWSYM OpF3       ; BBC direct page bit 7
      bbc 128
;;; asm2c:ENDSYM
NEWSYM Op04       ; OR A,dp   A <- A OR (dp)    N.....Z.
      SPCaddr_DP
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op14       ; OR A,dp+X    A <- A OR (dp+X)     N.....Z.
      SPCaddr_DP_X
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op24       ; AND A,dp     A <- A AND (dp)      N.....Z.
      SPCaddr_DP
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op34       ; AND A,dp+x   A <- A AND (dp+X)    N.....Z.
      SPCaddr_DP_X
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op44       ; EOR A,dp     A <- A EOR (dp)      N.....Z.
      SPCaddr_DP
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op54       ; EOR A,dp+x   A <- A EOR (dp+X)    N.....Z.
      SPCaddr_DP_X
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op64       ; CMP A,dp     A-(dp)               N.....ZC
      SPCaddr_DP
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op74       ; CMP A,dp+x   A-(dp+X)             N.....ZC
      SPCaddr_DP_X
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op84       ; ADC A,dp     A <- A+(dp)+C        NV..H.ZC
      SPCaddr_DP
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM Op94       ; ADC A,dp+x   A <- A+(dp+X)+C      NV..H.ZC
      SPCaddr_DP_X
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM OpA4       ; SBC A,dp     A <- A-(dp)-!C       NV..H.ZC
      SPCaddr_DP
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpB4       ; SBC A,dp+x   A <- A-(dp+X)-!C     NV..H.ZC
      SPCaddr_DP_X
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpC4       ; MOV dp,A     A -> (dp)            ........
      mov bl,[ebp]
      mov al, byte [spcA]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpD4       ; MOV dp+x,A   A -> (dp+X)          ........
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al, byte [spcA]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE4       ; MOV A,dp     A <- (dp)            N......Z
;      SPCaddr_DP_ReadByte
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM OpF4       ; MOV A,dp+x   A <- (dp+X)          N......Z
;      SPCaddr_DP_X_ReadByte
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM Op05       ; OR A,labs   A <- A OR (abs)       N.....Z.
;      SPCaddr_LABS
      mov bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op15       ; OR A,labs+x  A <- A OR (abs+X)    N.....Z.
      SPCaddr_LABS_X bx
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op25       ; AND A,labs   A <- A AND (abs)     N.....Z.
      SPCaddr_LABS
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op35       ; AND A,labs+X A <- A AND (abs+X)   N.....Z.
      SPCaddr_LABS_X bx
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op45       ; EOR A,labs   A <- A EOR (abs)     N.....Z.
      SPCaddr_LABS
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op55       ; EOR A,labs+X A <- A EOR (abs+X)   N.....Z.
      SPCaddr_LABS_X bl
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op65       ; CMP A,labs   A-(abs)              N.....ZC
      SPCaddr_LABS
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op75       ; CMP A,labs+X A-(abs+X)            N.....ZC
      SPCaddr_LABS_X bx
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op85       ; ADC A,labs   A <- A+(abs)+C       NV..H.ZC
      SPCaddr_LABS
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM Op95       ; ADC A,labs+X A <- A+(abs+X)+C     NV..H.ZC
      SPCaddr_LABS_X bl
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM OpA5       ; SBC A,labs   A <- A-(abs)-!C      NV..H.ZC
      SPCaddr_LABS
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpB5       ; SBC A,labs+x A <- A-(abs+X)-!C    NV..H.ZC
      SPCaddr_LABS_X bl
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpC5       ; MOV labs,A   A -> (abs)           ........
      mov bx,[ebp]
      mov al, byte [spcA]
      add ebp,2
      add ebx,spcRam
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpD5       ; MOV labs+X,A A -> (abs+X)         ........
      mov bl,[spcX]
      add bx,[ebp]
      mov al, byte [spcA]
      add ebp,2
      add ebx,spcRam
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE5       ; MOV A,labs   A <- (abs)           N......Z
;      SPCaddr_LABS_ReadByte
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM OpF5       ; MOV A,labs+X A <- (abs+X)         N......Z
;      SPCaddr_LABS_X_ReadByte
      mov bl,[spcX]
      add bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM Op06       ; OR A,(X)     A <- A OR (X)        N.....Z.
      SPCaddr__X_
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op16       ; OR A,labs+Y  A <- A OR (abs+Y)    N......Z.
      SPCaddr_LABS_Y
      SPC_OR_A al
;;; asm2c:ENDSYM
NEWSYM Op26       ; AND A,(X)    A <- A AND (X)       N......Z.
      SPCaddr__X_
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op36       ; AND A,labs+Y A <- A AND (abs+Y)   N......Z.
      SPCaddr_LABS_Y
      SPC_AND_A al
;;; asm2c:ENDSYM
NEWSYM Op46       ; EOR A,(X)    A <- A EOR (X)       N......Z.
      SPCaddr__X_
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op56       ; EOR A,labs+Y A <- A EOR (abs+Y)   N......Z.
      SPCaddr_LABS_Y
      SPC_EOR_A al
;;; asm2c:ENDSYM
NEWSYM Op66       ; CMP A,(X)    A-(X)                N......ZC
      SPCaddr__X_
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op76       ; CMP A,labs+Y A-(abs+Y)            N......ZC
      SPCaddr_LABS_Y
      SPC_CMP_A al
;;; asm2c:ENDSYM
NEWSYM Op86       ; ADC A,(X)    A <- A+(X)+C         NV..H..ZC
      SPCaddr__X_
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM Op96       ; ADC A,labs+Y A <- A+(abs+Y)+C     NV..H..ZC
      SPCaddr_LABS_Y
      SPC_ADC_A al
;;; asm2c:ENDSYM
NEWSYM OpA6       ; SBC A,(X)    A <- A-(X)-!C        NV..H..ZC
      SPCaddr__X_
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpB6       ; SBC A,labs+Y A <- A-(abs+Y)-!C    NV..H..ZC
;      SPCaddr_LABS_Y
      mov bl,[spcY]
      add bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      SPC_SBC_A al
;;; asm2c:ENDSYM
NEWSYM OpC6       ; MOV (X),A    A -> (X)             ........
      mov bl,[spcX]
      add ebx,[spcRamDP]
      mov al, byte [spcA]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpD6       ; MOV labs+Y,A A -> (abs+Y)         ........
      mov bl,[spcY]
      mov al, byte [spcA]
      add bx,[ebp]
      add ebp,2
      add ebx,spcRam
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE6       ; MOV A,(X)    A <- (X)             N......Z
;      SPCaddr__X__ReadByte
      mov bl,[spcX]
      add ebx,[spcRamDP]
      ReadByte
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM OpF6       ; MOV A,labs+Y A <- (abs+Y)         N......Z
;      SPCaddr_LABS_Y_ReadByte
      mov bl,[spcY]
      add bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM Op07       ; OR A,(dp+X)  A <- A OR ((dp+X+1)(dp+X))  N......Z.
      SPCaddr_bDP_Xb
      SPC_OR_A bl
;;; asm2c:ENDSYM
NEWSYM Op17       ; OR A,(dp)+Y  A <- A OR ((dp+1)(dp)+Y)   N......Z.
      SPCaddr_bDPb_Y
      SPC_OR_A bl
;;; asm2c:ENDSYM
NEWSYM Op27       ; AND A,(dp+X) A <- A AND ((dp+X+1)(dp+X)) N......Z.
      SPCaddr_bDP_Xb
      SPC_AND_A bl
;;; asm2c:ENDSYM
NEWSYM Op37       ; AND A,(dp)+Y A <- A AND ((dp+1)(dp)+Y)  N......Z.
      SPCaddr_bDPb_Y
      SPC_AND_A bl
;;; asm2c:ENDSYM
NEWSYM Op47       ; EOR A,(dp+X) A <- A EOR ((dp+X+1)(dp+X)) N......Z.
      SPCaddr_bDP_Xb
      SPC_EOR_A bl
;;; asm2c:ENDSYM
NEWSYM Op57       ; EOR A,(dp)+Y A <- A EOR ((dp+1)(dp)+Y)  N......Z.
      SPCaddr_bDPb_Y
      SPC_EOR_A bl
;;; asm2c:ENDSYM
NEWSYM Op67       ; CMP A,(dp+X) A-((dp+X+1)(dp+X))      N......ZC
      SPCaddr_bDP_Xb
      SPC_CMP_A bl
;;; asm2c:ENDSYM
NEWSYM Op77       ; CMP A,(dp)+Y A-((dp+1)(dp)+Y)        N......ZC
      SPCaddr_bDPb_Y
      SPC_CMP_A bl
;;; asm2c:ENDSYM
NEWSYM Op87       ; ADC A,(dp+X) A <- A+((dp+X+1)(dp+X)) NV..H..ZC
      SPCaddr_bDP_Xb
      SPC_ADC_A bl
;;; asm2c:ENDSYM
NEWSYM Op97       ; ADC A,(dp)+Y A <- A+((dp+1)(dp)+Y)   NV..H..ZC
;      SPCaddr_bDPb_Y
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      mov ax,[ebx]
      inc ebp
      add ax,[spcY]
      mov bl,[spcRam+eax]
      SPC_ADC_A bl
;;; asm2c:ENDSYM
NEWSYM OpA7       ; SBC A,(dp+X) A <- A-((dp+X+1)(dp+X))-!C NV..H..ZC
      SPCaddr_bDP_Xb
      SPC_SBC_A bl
;;; asm2c:ENDSYM
NEWSYM OpB7       ; SBC A,(dp)+Y A <- A-((dp+1)(dp)+Y)-!C   NV..H..ZC
;      SPCaddr_bDPb_Y
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      mov ax,[ebx]
      inc ebp
      add ax,[spcY]
      mov bl,[spcRam+eax]
      SPC_SBC_A bl
;;; asm2c:ENDSYM
NEWSYM OpC7       ; MOV (dp+X),A A -> ((dp+X+1)(dp+X))     ........
      mov bl,[ebp]
      add bl,[spcX]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      mov ebx,eax
      add ebx,spcRam
      mov al, byte [spcA]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpD7       ; MOV (dp)+Y,A A -> ((dp+1)(dp)+Y)       ........
      mov bl,[ebp]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      add ax,[spcY]
      mov ebx,eax
      add ebx,spcRam
      mov al, byte [spcA]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE7       ; MOV A,(dp+X) A <- ((dp+X+1)(dp+X))     N......Z
;      SPCaddr_bDP_Xb_ReadByte
      mov bl,[ebp]
      add bl,[spcX]
      xor eax,eax
      add ebx,[spcRamDP]
      inc ebp
      mov ax, word [ebx]
      mov ebx,eax
      add ebx,spcRam
      ReadByte
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM OpF7       ; MOV A,(dp)+Y A <- ((dp+1)(dp)+Y)       N......Z
;      SPCaddr_bDPb_Y_ReadByte
      xor eax,eax
      mov al,[ebp]
      add eax,[spcRamDP]
      inc ebp
      mov bx,[eax]
      add bx,[spcY]
      add ebx,spcRam
      ReadByte
      SPC_MOV_A
;;; asm2c:ENDSYM
NEWSYM Op08       ; OR A,#inm    A <- A OR inm            N......Z.
      mov bl,[ebp]
      inc ebp
      or byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op18       ; OR dp,#inm   (dp) <- (dp) OR inm         N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      or al,ah
      mov [spcNZ],al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op28       ; AND A,#inm   A <- A AND inm           N......Z.
      mov bl,[ebp]
      inc ebp
      and byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op38       ; AND dp,#inm  (dp) <- (dp) AND inm        N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      and al,ah
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op48       ; EOR A,#inm   A <- A EOR inm           N......Z.
      mov bl,[ebp]
      inc ebp
      xor byte [spcA], bl
      mov al,[spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op58      ; EOR dp,#inm  (dp) <- (dp) EOR inm        N......Z.
      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      xor al,ah
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op68       ; CMP A,#inm   A-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp byte [spcA], bl
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op78       ; CMP dp,#inm  (dp)-inm                N......ZC
      mov bl,[ebp+1]
      add ebx,[spcRamDP]
      mov ah,[ebp]
      mov al,[ebx]
      add ebp,2
      cmp al,ah
      cmc
      SPCSetFlagnzcnoret
      ret
;;; asm2c:ENDSYM
NEWSYM Op88       ; ADC A,#inm   A <- A+inm+C            NV..H..ZC
      mov bl,[ebp]
      inc ebp
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc byte [spcA], bl
      SPCSetFlagnvhzc
;;; asm2c:ENDSYM
NEWSYM Op98       ; ADC dp,#inm  (dp) <- (dp)+inm+C         NV..H..ZC
;      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      mov bl,[ebp+1]
      mov ah,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      add ebp,2
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc al, ah
      SPCSetFlagnvhzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpA8       ; SBC A,#inm   A <- A-inm-!C           NV..H..ZC
      mov bl,[ebp]
      inc ebp
;      SPC_SBC_A
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb byte [spcA], bl
      cmc
      SPCSetFlagnvhzc
;;; asm2c:ENDSYM
NEWSYM OpB8       ; SBC dp,#inm  (dp) <- (dp)-inm-!C        NV..H..ZC
;      spcgetdp_imm ; bl<-[ebp+1], ah<-[ebp], ebx+[spcRamDP],Readbyte,ebp+2
      mov bl,[ebp+1]
      mov ah,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      add ebp,2
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb al,ah
      cmc
      SPCSetFlagnvhzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpC8       ; CMP X,#inm   X-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp [spcX],bl
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpD8       ; MOV dp,X     X -> (dp)                 ........
      mov bl,[ebp]
      mov al, byte [spcX]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE8       ;  MOV A,#inm  A <- inm                  N......Z
      mov bl,[ebp]
      inc ebp
;      SPC_MOV_A
      mov byte [spcA], bl
      mov al,bl
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpF8       ;  MOV X,dp    X <- (dp)                 N......Z
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      mov byte [spcX], al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op09       ; OR dp(d),dp(s)  (dp(d))<-(dp(d)) OR (dp(s))  N......Z.
      spcaddrDPbDb_DPbSb
      or al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op19       ; OR (X),(Y)   (X) <- (X) OR (Y)          N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      or [ebx],al
      mov al,[ebx]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op29       ; AND dp(d),dp(s) (dp(d))<-(dp(d)) AND (dp(s)) N......Z.
      spcaddrDPbDb_DPbSb
      and al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op39       ; AND (X),(Y)  (X) <- (X) AND (Y)         N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      and [spcRam+ebx], al
      mov al,[spcRam+ebx]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op49       ; EOR dp(d),dp(s) (dp(d))<-(dp(d)) EOR (dp(s)) N......Z.
      spcaddrDPbDb_DPbSb
      xor al,ah
      mov [spcNZ], al
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op59       ; EOR (X),(Y)  (X) <- (X) EOR (Y)         N......Z.
;      spcaddrDPbXb_bYb
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      xor [ebx], al
      mov al,[ebx]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op69       ; CMP dp(d),dp(s) (dp(d))-(dp(s))         N......ZC
      spcaddrDPbDb_DPbSb
      cmp al,ah
      cmc
      SPCSetFlagnzcnoret
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op79       ; CMP (X),(Y)  (X)-(Y)                 N......ZC
;      spcaddrDPbXb_bYb Op79b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      cmp [ebx], al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op89       ; ADC dp(d),dp(s) (dp(d))<-(dp(d))+(dp(s))+C  NV..H..ZC
      spcaddrDPbDb_DPbSb
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc al, ah
      SPCSetFlagnvhzcnoret
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op99       ; ADC (X),(Y)  (X) <- (X)+(Y)+C          NV..H..ZC
;      spcaddrDPbXb_bYb Op99b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      test byte[spcP],01h
      clc
      jz .carryClear
      stc
.carryClear
      adc [ebx],al
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpA9       ; SBC dp(d),dp(s) (dp(d))<-(dp(d))-(dp(s))-!C NV..H..ZC
      spcaddrDPbDb_DPbSb
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb al,ah
      SPCSetFlagnvhzcnoret
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM OpB9       ; SBC (X),(Y)  (X) <- (X)-(Y)-!C         NV..H..ZC
;      spcaddrDPbXb_bYb OpB9b:
      mov bl,[spcY]
      add ebx,[spcRamDP]
      mov al,[ebx]
      xor ebx,ebx
      mov bl,[spcX]
      add ebx,[spcRamDP]
      test byte[spcP],01h
      clc
      jnz .carrySet
      stc
.carrySet
      sbb [ebx],al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpC9       ; MOV labs,X   X -> (abs)                ........
      mov bx,[ebp]
      mov al, byte [spcX]
      add ebp,2
      add ebx,spcRam
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpD9       ; MOV dp+Y,X   X -> (dp+Y)               ........
      mov bl,[ebp]
      mov al, byte [spcX]
      add bl,[spcY]
      inc ebp
      add ebx,[spcRamDP]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpE9       ; MOV X,labs   X <- (abs)                N......Z
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      mov byte [spcX], al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpF9       ; MOV X,dp+Y   X <- (dp+Y)               N......Z
      mov bl,[ebp]
      add bl,[spcY]
      inc ebp
      add ebx,[spcRamDP]
      ReadByte
      mov byte [spcX], al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op0A       ; OR1 C,mem.bit   C <- C OR  (mem.bit)      ........C
      spcaddrmembit
      and al,01h
      or [spcP],al
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op1A       ; DECW dp   Decrement dp memory pair  N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      dec ax
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp .SkipFlag
.YesNeg
      mov byte [spcNZ],80h
      jmp .SkipFlag
.YesZero
      mov byte [spcNZ],0
      jmp .SkipFlag
.SkipFlag
      push ebx
      WriteByte
      pop ebx
      inc ebx
      mov al,ah
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op2A       ; OR1 C,/mem.bit  C <- C OR  !(mem.bit)     ........C
      spcaddrmembit
      and al,01h
      xor al,01h
      or [spcP],al
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op3A       ; INCW dp   Increment dp memory pair  N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      inc ax
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      jmp .SkipFlag
.YesNeg
      mov byte [spcNZ],80h
      jmp .SkipFlag
.YesZero
      mov byte [spcNZ],0
      jmp .SkipFlag
.SkipFlag
      push ebx
      WriteByte
      pop ebx
;;; asm2c:ENDSYM
NEWSYM Op3Ab
      inc ebx
      mov al,ah
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op4A       ; AND1 C,mem.bit  C <- C AND (mem.bit)      ........C
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
      or al,0FEh
      and [spcP],al
      xor ecx,ecx
      ret
; looks like there is the Carry flag checked in op5a..
;;; asm2c:ENDSYM
NEWSYM Op5A       ; CMPW YA,dp   YA - (dp+1)(dp)        N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      cmp bx,ax
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op6A       ; AND1 C,/mem.bit C <- C AND !(mem.bit)     ........C
      mov bx,[ebp]

      mov cl,bl
      add ebp,2
      shr bx,3
      and cl,7
      mov al,[spcRam+ebx]
      shr al,cl
      or al,0FEh
      xor al,01h
      and [spcP],al
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op7A       ; ADDW YA,dp   YA  <- YA + (dp+1)(dp)   NV..H..ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      add bx,ax
      mov [spcA],bl
      mov [spcY],bh
      SPCSetFlagnvhzc
;;; asm2c:ENDSYM
NEWSYM Op8A       ; EOR1 C,mem.bit  C <- C EOR (mem.bit)      ........C
      spcaddrmembit
      and al,01h
      xor [spcP],al
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op9A       ; SUBW YA,dp   YA  <- YA - (dp+1)(dp)   NV..H..ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov bl,[spcA]
      mov bh,[spcY]
      sub bx,ax
      cmc
      mov [spcA],bl
      mov [spcY],bh
      SPCSetFlagnvhzc
;;; asm2c:ENDSYM
NEWSYM OpAA       ; MOV1 C,mem.bit  C <- (mem.bit)
      spcaddrmembit
      and byte[spcP],0FEh
      and al,01h
      or [spcP],al
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM OpBA       ; MOVW YA,dp   YA  - (dp+1)(dp)       N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov ax,[ebx]
      mov [spcA],al
      mov [spcY],ah
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      ret
.YesNeg
      mov byte [spcNZ],80h
      ret
.YesZero
      mov byte [spcNZ],0
      ret
;;; asm2c:ENDSYM
NEWSYM OpCA       ; MOV1 mem.bit,C  C -> (mem.bit)            .........
      push edx
      mov bx,[ebp]
      mov al,[spcP]

      mov cl,bl
      mov ah,01h
      and cl,7
      shr bx,3
      shl ah,cl
      and al,01h
      add ebp,2
      shl al,cl
      mov ecx,spcRam
      ; al = carry flag positioned in correct location, ah = 1 positioned
      xor ah,0FFh
      add ecx,ebx
      mov dl,[ecx]
      and dl,ah
      or dl,al
      mov al,dl
      mov ebx,ecx
      WriteByte
      pop edx
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM OpDA       ; MOVW dp,YA   (dp+1)(dp) - YA         .........
      mov bl,[ebp]
      inc ebp
      add ebx,[spcRamDP]
      mov al,[spcA]
      push ebx
      WriteByte
      pop ebx
      inc ebx
      mov al,[spcY]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpEA       ; NOT1 mem.bit    complement (mem.bit)      .........
      push edx
      mov bx,[ebp]

      mov cl,bl
      mov ah,01h
      and cl,7
      shr bx,3
      shl ah,cl
      mov ecx,spcRam
      add ebp,2
      add ecx,ebx
      mov dl,[ecx]
      xor dl,ah
      mov al,dl
      mov ebx,ecx
      WriteByte
      pop edx
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM OpFA       ; MOV dp(d),dp(s) (dp(d)) <- (dp(s))        ........
      xor ecx,ecx
      mov bl,[ebp+1]
      mov cl,[ebp]
      add ebx,[spcRamDP]
      add ecx,[spcRamDP]
      add ebp,2
      mov al,[ecx]
      WriteByte
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op0B       ; ASL dp    C << (dp)   <<0       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op1B       ; ASL dp+X  C << (dp+X) <<0       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op2B       ; ROL dp    C << (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op2Bb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      ret
.Op2Bb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op3B       ; ROL dp+X  C << (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op3Bb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      ret
.Op3Bb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op4B       ; LSR dp    0 >> (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op5B       ; LSR dp+X  0 >> (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      inc ebp
      add ebx,[spcRamDP]
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op6B       ; ROR dp    C >> (dp)   <<C       N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op6Bb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      ret
.Op6Bb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op7B       ; ROR dp+X  C >> (dp+X) <<C       N......ZC
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      test byte [spcP],01h
      jnz near .Op7Bb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      ret
.Op7Bb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op8B       ;  DEC dp   -- (dp)               N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op9B       ;  DEC dp+X -- (dp+X)             N......Z.
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpAB       ; INC dp    ++ (dp)               N......Z.
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpBB       ; INC dp+X  ++ (dp+X)             N......Z.
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpCB       ; MOV dp,Y  Y -> (dp)                 ........
      mov bl,[ebp]
      mov al, byte [spcY]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpDB       ; MOV dp+X,Y   X -> (dp+X)               ........
      mov bl,[ebp]
      add bl,[spcX]
      mov al, byte [spcY]
      add ebx,[spcRamDP]
      inc ebp
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpEB       ; MOV Y,dp  Y <- (dp)                 N......Z
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      ReadByte
      mov byte [spcY], al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpFB       ; MOV Y,dp+X   Y <- (dp+X)               N......Z
      mov bl,[ebp]
      add bl,[spcX]
      inc ebp
      add ebx,[spcRamDP]
      ReadByte
      mov byte [spcY], al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op0C       ; ASL labs  C << (abs)  <<0       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      shl al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op1C       ; ASL A  C << A      <<0       N......ZC
      shl byte [spcA],1
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op2C       ; ROL labs  C << (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      test byte [spcP],01h
      jnz near .Op2Cb
      mov al,[ebx]
      clc
      spcROLstuff
      WriteByte
      ret
.Op2Cb
      mov al,[ebx]
      stc
      spcROLstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op3C       ; ROL A  C << A      <<C       N......ZC
      test byte [spcP],01h
      jnz near .Op3Cb
      clc
      rcl byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      ret
.setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
      ret
.Op3Cb
      stc
      rcl byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag2
      and byte[spcP],0FEh
      mov [spcNZ],al
      ret
.setcarryflag2
      or byte[spcP],01h
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op4C       ; LSR labs  0 >> (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      shr al,1
      SPCSetFlagnzcnoret
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op5C       ; LSR A  0 >> A      <<C       N......ZC
      shr byte [spcA],1
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op6C       ; ROR labs  C >> (abs)  <<C       N......ZC
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      test byte [spcP],01h
      jnz near .Op6Cb
      mov al,[ebx]
      clc
      spcRORstuff
      WriteByte
      ret
.Op6Cb
      mov al,[ebx]
      stc
      spcRORstuff
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op7C       ; ROR A  C >> A      <<C       N......ZC
      test byte [spcP],01h
      jnz near .Op7Cb
      clc
      rcr byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag
      and byte[spcP],0FEh
      mov [spcNZ],al
      ret
.setcarryflag
      or byte[spcP],01h
      mov [spcNZ],al
      ret
.Op7Cb
      stc
      rcr byte [spcA],1
      mov al,[spcA]
      jc .setcarryflag2
      and byte[spcP],0FEh
      mov [spcNZ],al
      ret
.setcarryflag2
      or byte[spcP],01h
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op8C       ; DEC labs  -- (abs)              N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      dec al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op9C       ; DEC A  -- A                  N......Z.
      dec byte [spcA]
      mov al,byte [spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpAC       ; INC labs  ++ (abs)              N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      inc al
      mov [spcNZ], al
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpBC       ; INC A  ++ A                  N......Z.
      inc byte [spcA]
      mov al,byte [spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpCC       ; MOV labs,Y   Y -> (abs)                ........
      mov bx,[ebp]
      mov al, byte [spcY]
      add ebp,2
      add ebx,spcRam
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpDC       ; DEC Y  -- Y                  N......Z.
      dec byte [spcY]
      mov al,byte [spcY]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpEC       ; MOV Y,labs   Y <- (abs)                N......Z
      mov bx,[ebp]
      add ebx,spcRam
      ReadByte
      add ebp,2
      mov byte [spcY],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpFC       ; INC Y  ++ Y                  N......Z.
      inc byte [spcY]
      mov al,byte [spcY]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op0D       ; PUSH PSW     push PSW to stack       .........
      mov eax,[spcS]
      mov bl,[spcP]
      and bl,01111101b
      test byte [spcNZ],80h
      jnz .NegSet
      cmp byte [spcNZ],0
      je .ZeroSet
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
.NegSet
      or bl,80h
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
.ZeroSet
      or bl,02h
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
;;; asm2c:ENDSYM
NEWSYM Op1D       ; DEC X     -- X                  N......Z.
      dec byte [spcX]
      mov al,byte [spcX]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op2D       ; PUSH A       push A to stack         .........
      mov eax,[spcS]
      mov bl,[spcA]
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
;;; asm2c:ENDSYM
NEWSYM Op3D       ; INC X     ++ X                  N......Z.
      inc byte [spcX]
      mov al,byte [spcX]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op4D       ; PUSH X       push X to stack         .........
      mov eax,[spcS]
      mov bl,[spcX]
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
;;; asm2c:ENDSYM
NEWSYM Op5D       ; MOV X,A      X <- A                   N......Z
      mov al,[spcA]
      mov [spcX],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op6D       ; PUSH Y    push Y to stack         .........
      mov eax,[spcS]
      mov bl,[spcY]
      dec byte [spcS]
      mov [spcRam+eax],bl
      ret
;;; asm2c:ENDSYM
NEWSYM Op7D       ; MOV A,X      A <- X                   N......Z
      mov al,[spcX]
      mov [spcA],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op8D       ; MOV Y,#inm   Y <- inm                  N......Z
      mov bl,[ebp]
      mov [spcY],bl
      inc ebp
      mov al,bl
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op9D       ; MOV X,SP     X <- SP                  N......Z
      mov al,[spcS]
      mov [spcX],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpAD       ; CMP Y,#inm   Y-inm                   N......ZC
      mov bl,[ebp]
      inc ebp
      cmp [spcY],bl
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpBD       ; MOV SP,X     SP <- X                   ........
      mov al,[spcX]
      mov [spcS],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpCD       ; MOV X,#inm   X <- inm                  N......Z
      mov bl,[ebp]
      mov [spcX],bl
      inc ebp
      mov al,bl
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpDD       ; MOV A,Y      A <- Y                   N......Z
      mov al,[spcY]
      mov [spcA],al
      mov [spcNZ],al
      ret
;  NOTC               ED    1     3   complement carry flag     .......C 
;;; asm2c:ENDSYM
NEWSYM OpED       ; NOTC         complement carry flag     .......C 
      xor byte [spcP],00000001b
      ret
;;; asm2c:ENDSYM
NEWSYM OpFD       ; MOV Y,A      Y <- A                   N......Z
      mov al,[spcA]
      mov [spcY],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM Op0E       ; TSET1 labs   test and set bits with A   N......Z.
      mov bx,[ebp]
      add ebx,spcRam
      add ebp,2
      mov al,[ebx]
      mov ah,al
      and ah,[spcA]
      mov [spcNZ],ah
      or al,[spcA]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op1E       ; CMP X,labs   X-(abs)                 N......ZC
      mov bx,[ebp]
      add ebp,2
      mov al,[spcRam+ebx]
      cmp byte [spcX], al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op2E       ; CBNE dp,rel  compare A with (dp) then BNE   ...
      mov bl,[ebp]
      add ebx,[spcRamDP]
      mov al,[ebx]
      cmp byte [spcA], al
      jne .Jump
      add ebp,2
      ret
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      ret
;;; asm2c:ENDSYM
NEWSYM Op3E       ; CMP X,dp     X-(dp)                  N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      cmp byte [spcX], al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op4E       ; TCLR1     test and clear bits with A N......Z.
      mov bx,[ebp]
      add ebx,[spcRamDP]
      add ebp,2
      mov al,[ebx]
      mov ah,al
      and ah,[spcA]
      mov [spcNZ],ah
      mov ah,[spcA]
      not ah
      and al,ah
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op5E       ; CMP Y,labs   Y-(abs)                 N......ZC
      mov bx,[ebp]
      mov al,[spcRam+ebx]
      add ebp,2
      cmp byte [spcY], al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op6E       ; DBNZ   decrement memory (dp) then JNZ ...
      mov bl,[ebp]
      add ebx,[spcRamDP]
      dec byte[ebx]
      jnz .Jump
      add ebp,2
      ret
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      ret
;;; asm2c:ENDSYM
NEWSYM Op7E       ; CMP Y,dp     Y-(dp)                  N......ZC
      mov bl,[ebp]
      add ebx,[spcRamDP]
      inc ebp
      mov al,[ebx]
      cmp byte [spcY], al
      cmc
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM Op8E       ; POP PSW   pop PSW from stack     (Restored)
      mov byte [spcNZ],0
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcP],bl
      test byte [spcP],2
      jz .ZeroNo
      mov byte [spcNZ],0
      jmp .DoNeg
.ZeroNo
      or byte [spcNZ],1
.DoNeg
      test byte [spcP],80h
      jz .NoNeg
      or byte [spcNZ],80h
.NoNeg
      mov dword [spcRamDP],spcRam
      test byte[spcP],32
      jnz .setpage1
      ret
.setpage1
      add dword [spcRamDP],100h
      ret
;;; asm2c:ENDSYM
NEWSYM Op9E       ; DIV YA,X     Q:A B:Y <- YA / X       NV..H..Z.
   push edx
   mov ah,[spcY]
   mov al,[spcA]
   xor bh,bh
   xor dx,dx
   mov bl,[spcX]
   cmp bl,0
   je NoDiv
   div bx
   mov byte[spcA],al
   mov byte[spcY],dl
   cmp ah,0
   jne Over
   and byte [spcP],191-16
   pop edx
   mov [spcNZ],al
   ret
NoDiv:
   mov byte [spcA],0ffh
   mov byte [spcY],0ffh
   or byte [spcP],16
   and byte[spcP],255-64
   pop edx
   ret
Over:
   or byte [spcP],64
   and byte[spcP],255-16
   pop edx
   mov [spcNZ],al
   ret
;;; asm2c:ENDSYM
NEWSYM OpAE       ; POP A     pop A from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcA],bl
      ret
;;; asm2c:ENDSYM
NEWSYM OpBE       ; DAS A     decimal adjust for sub  N......ZC
;      ret
      ; copy al flags into AH
      xor ah,ah
      test byte[spcNZ],80h
      jz .noneg
      or ah,10000000b
.noneg
      test byte[spcP],01h
      jz .nocarry
      or ah,00000001b
.nocarry
      test byte[spcNZ],0FFh
      jnz .nozero
      or ah,01000000b
.nozero
      test byte[spcP],08h
      jz .nohcarry
      or ah,00010000b
.nohcarry
      mov al,[spcA]
      sahf
      das
      mov [spcA],al
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpCE       ; POP X     pop X from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcX],bl
      ret
;;; asm2c:ENDSYM
NEWSYM OpDE       ; CBNE dp+X,rel   compare A with (dp+X) then BNE ...
      mov bl,[ebp]
      add bl,[spcX]
      add ebx,[spcRamDP]
      mov al,[ebx]
      cmp byte [spcA], al
      jne .Jump
      add ebp,2
      ret
.Jump
      movsx ebx,byte [ebp+1]
      add ebp,ebx
      add ebp,2
      ret
;;; asm2c:ENDSYM
NEWSYM OpEE       ; POP Y     pop Y from stack        .........
      inc byte [spcS]
      mov eax,[spcS]
      mov bl,[spcRam+eax]
      mov [spcY],bl
      ret
;;; asm2c:ENDSYM
NEWSYM OpFE       ; DBNZ Y,rel   decrement Y then JNZ           ...
      dec byte [spcY]
      jnz .Jump
      inc ebp
      ret
.Jump
      movsx ebx,byte [ebp]
      add ebp,ebx
      inc ebp
      ret
;;; asm2c:ENDSYM
NEWSYM Op0F       ; BRK       software interrupt       ...1.0.. 
;      inc dword[spc700read]
      dec ebp
      ret
;;; asm2c:ENDSYM
NEWSYM Op1F       ; JMP (labs+X)    PC <- (abs+X+1)(abs+X)         ...
      mov bx,[ebp]
      add bx,[spcX]
      xor eax,eax
      add ebp,2
      mov ax, word [spcRam+ebx]
      mov ebp,spcRam
      add ebp,eax
      ret
;;; asm2c:ENDSYM
NEWSYM Op2F       ; BRA rel      branch always                  ...
      movsx ebx,byte [ebp]
      inc ebp
      add ebp,ebx
      ret
;;; asm2c:ENDSYM
NEWSYM Op3F       ; CALL labs    subroutine call          ........
      ; calculate PC
      mov ecx,ebp
      add ecx,2
      sub ecx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],ch
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],cl
      dec byte [spcS]
      ; set new PC
      mov cx,[ebp]
      add ecx,spcRam
      mov ebp,ecx
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op4F       ; PCALL upage  upage call               ........ 
      ; calculate PC
      mov ecx,ebp
      inc ecx
      sub ecx,spcRam
      mov eax,[spcS]
      mov [spcRam+eax],ch
      dec byte [spcS]
      mov eax,[spcS]
      mov [spcRam+eax],cl
      dec byte [spcS]
      ; set new PC
      xor ecx,ecx
      mov cl,[ebp]
      add ecx,spcRam
      add ecx,0ff00h
      mov ebp,ecx
      xor ecx,ecx
      ret
; I'm not sure about this one and JMP labs+X...
;;; asm2c:ENDSYM
NEWSYM Op5F       ; JMP labs     jump to new location           ...
      mov bx,[ebp]
      add ebp,2
      mov ebp,spcRam
      add ebp,ebx
      ret
;;; asm2c:ENDSYM
NEWSYM Op6F       ; ret          ret from subroutine   ........
      xor ecx,ecx
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      inc byte [spcS]
      mov eax,[spcS]
      mov ch,[spcRam+eax]
      add ecx,spcRam
      mov ebp,ecx
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op7F       ; ret1         return from interrupt   (Restored)
      xor ecx,ecx
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      mov [spcP],cl
      test byte [spcP],80h
      jz .NoNeg
      or byte [spcNZ],80h      
.NoNeg
      test byte [spcP],2
      jz .NoZero
      mov byte [spcNZ],0
      jmp .YesZero
.NoZero
      or byte [spcNZ],1
.YesZero
      inc byte [spcS]
      mov eax,[spcS]
      mov cl,[spcRam+eax]
      inc byte [spcS]
      mov eax,[spcS]
      mov ch,[spcRam+eax]
      add ecx,spcRam
      mov ebp,ecx
      ; set direct page
      mov dword [spcRamDP],spcRam
      test byte[spcP],32
      jz .nodp
      add dword [spcRamDP],100h
.nodp
      xor ecx,ecx
      ret
;;; asm2c:ENDSYM
NEWSYM Op8F       ; MOV dp,#inm  (dp) <- inm               ........
      mov bl,[ebp+1]
      mov al,[ebp]
      add ebx,[spcRamDP]
      add ebp,2
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM Op9F       ; XCN A     A(7-4) <-> A(3-0)     N......Z.
      ror byte [spcA],4
      mov al,byte[spcA]
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpAF       ; MOV (X)+,A   A -> (X) with auto inc    ........
      mov bl,[spcX]
      add ebx,[spcRamDP]
      mov al, byte [spcA]
      inc byte [spcX]
      WriteByte
      ret
;;; asm2c:ENDSYM
NEWSYM OpBF       ; MOV A,(X)+  A <- (X) with auto inc    N......Z
      mov bl,[spcX]
      add ebx,[spcRamDP]
      ReadByte
      inc byte [spcX]
      mov byte [spcA],al
      mov [spcNZ],al
      ret
;;; asm2c:ENDSYM
NEWSYM OpCF       ; MUL YA       YA(16 bits) <- Y * A    N......Z.
      mov al,[spcY]
      mov bl,[spcA]
      mul bl
      mov [spcA],al
      mov [spcY],ah
      ; ??? (Is the n flag set on YA or A?)
      test ax,8000h
      jnz .YesNeg
      cmp ax,0000h
      je .YesZero
      mov byte [spcNZ],1
      ret
.YesNeg
      mov byte [spcNZ],80h
      ret
.YesZero
      mov byte [spcNZ],0
      ret
;;; asm2c:ENDSYM
NEWSYM OpDF       ; DAA A        decimal adjust for add  N......ZC
      ; copy al flags into AH
      xor ah,ah
      test byte[spcNZ],80h
      jz .noneg
      or ah,10000000b
.noneg
      test byte[spcP],01h
      jz .nocarry
      or ah,00000001b
.nocarry
      test byte[spcNZ],0FFh
      jnz .nozero
      or ah,01000000b
.nozero
      test byte[spcP],08h
      jz .nohcarry
      or ah,00010000b
.nohcarry
      mov al,[spcA]
      sahf
      daa
      mov [spcA],al
      SPCSetFlagnzc
;;; asm2c:ENDSYM
NEWSYM OpEF       ; SLEEP        standby SLEEP mode      .........
      dec ebp
      ret
;;; asm2c:ENDSYM
NEWSYM OpFF       ; STOP         standby STOP mode       .........
;      inc dword[spc700read]
      dec ebp
      ret
;;; asm2c:ENDSYM


