/*
 * Copyright (C) 2023 David Hunter
 *
 * This file is part of the zsnes-asm2c project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdint.h>
#include <unistd.h>
#include "symtab.h"

typedef union {
    uint32_t r32;
    uint16_t r16;
    struct {
        uint8_t l;
        uint8_t h;
    } r8;
} reg32_t;

typedef union {
    struct {
        uint32_t cf : 1;                        // implemented
        uint32_t _b1 : 1;
        uint32_t pf : 1;
        uint32_t _b3 : 1;
        uint32_t af : 1;
        uint32_t _b5 : 1;
        uint32_t zf : 1;                        // implemented
        uint32_t sf : 1;                        // implemented
        uint32_t tf : 1;
        uint32_t ie : 1;
        uint32_t df : 1;
        uint32_t of : 1;                        // implemented
        uint32_t iopl : 2;
        uint32_t nt : 1;
        uint32_t _b15 : 1;
        uint32_t rf : 1;
        uint32_t vm : 1;
    } v;
    uint32_t r;
} flags_t;

typedef struct {
    reg32_t _eax;
    reg32_t _ebx;
    reg32_t _ecx;
    reg32_t _edx;
    reg32_t _esp;
    reg32_t _ebp;
    reg32_t _esi;
    reg32_t _edi;
    flags_t _flg;
} state_t;

extern uint8_t* _ram;
extern state_t _st;

#define eax _st._eax.r32
#define ebx _st._ebx.r32
#define ecx _st._ecx.r32
#define edx _st._edx.r32
#define esp _st._esp.r32
#define ebp _st._ebp.r32
#define esi _st._esi.r32
#define edi _st._edi.r32

#define ax _st._eax.r16
#define bx _st._ebx.r16
#define cx _st._ecx.r16
#define dx _st._edx.r16
#define sp _st._esp.r16
#define bp _st._ebp.r16
#define si _st._esi.r16
#define di _st._edi.r16

#define al _st._eax.r8.l
#define bl _st._ebx.r8.l
#define cl _st._ecx.r8.l
#define dl _st._edx.r8.l

#define ah _st._eax.r8.h
#define bh _st._ebx.r8.h
#define ch _st._ecx.r8.h
#define dh _st._edx.r8.h

#define EFLAGS _st._flg.v

#define NEWSYM(fn)              void fn(void)

#define m_dword(a)              *(uint32_t*)&_ram[a]
#define m_word(a)               *(uint16_t*)&_ram[a]
#define m_byte(a)               _ram[a]

#define op2_mov(op1, op2)       op1 = op2

static inline uint32_t _op2_movsx(uint32_t op2, size_t w)
{
    int32_t smask = 1ULL << (w * 8 - 1);
    int32_t tmp = op2;
    tmp |= ~((op2 & smask) - 1);                // sign-extend
    return tmp;
}

#define op2_movsx(op1, op2)     op1 = _op2_movsx(op2, sizeof(op2))


static inline void uf_logic(uint32_t op1, size_t w)
{
    EFLAGS.of = 0;
    EFLAGS.cf = 0;
    EFLAGS.sf = (op1 & (1 << (w - 1))) != 0;
    EFLAGS.zf = op1 == 0;
}

static inline uint32_t _op2_and(uint32_t op1, uint32_t op2, size_t w)
{
    op1 = op1 & op2;
    uf_logic(op1, w);
    return op1;
}

static inline uint32_t _op2_or(uint32_t op1, uint32_t op2, size_t w)
{
    op1 = op1 | op2;
    uf_logic(op1, w);
    return op1;
}

static inline uint32_t _op2_xor(uint32_t op1, uint32_t op2, size_t w)
{
    op1 = op1 ^ op2;
    uf_logic(op1, w);
    return op1;
}

#define op2_and(op1, op2)       op1 = _op2_and(op1, op2, sizeof(op1))
#define op2_or(op1, op2)        op1 = _op2_or(op1, op2, sizeof(op1))
#define op2_xor(op1, op2)       op1 = _op2_xor(op1, op2, sizeof(op1))
#define op2_test(op1, op2)      (void)_op2_and(op1, op2, sizeof(op1))
#define op1_not(op1)            op1 = ~op1


static inline uint32_t _op2_addsub(uint32_t op1, uint64_t op2, unsigned cf,
                                   size_t w)
{
    uint64_t tmp = (uint64_t)op1 + op2 + cf;
    uint64_t cmask = 1ULL << (w * 8);
    uint64_t smask = 1ULL << (w * 8 - 1);
    EFLAGS.of = ((op1 & smask) == (op2 & smask)) &&
        ((tmp & smask) != (op1 & smask));
    EFLAGS.cf = (tmp & cmask) != 0;
    EFLAGS.sf = (tmp & smask) != 0;
    tmp = tmp & (cmask - 1);
    EFLAGS.zf = tmp == 0;
    return tmp;
}

static inline uint32_t _op2_add(uint32_t op1, uint32_t op2, unsigned cf,
                                size_t w)
{
    EFLAGS.af = (op1 & 0xf) + (op2 & 0xf) + cf > 0xf;
    return _op2_addsub(op1, op2, cf, w);
}

static inline uint32_t _op2_sub(uint32_t op1, uint32_t op2, unsigned cf,
                                size_t w)
{
    uint64_t nop2 = ~(uint64_t)op2;
    EFLAGS.af = (op1 & 0xf) + (nop2 & 0xf) + cf <= 0xf;
    return _op2_addsub(op1, nop2, cf, w);
}

#define op2_add(op1, op2)       op1 = _op2_add(op1, op2, 0, sizeof(op1))
#define op2_adc(op1, op2)       op1 = _op2_add(op1, op2, EFLAGS.cf, sizeof(op1))
#define op2_sub(op1, op2)       op1 = _op2_sub(op1, op2, 1, sizeof(op1))
#define op2_sbb(op1, op2)       op1 = _op2_sub(op1, op2, !EFLAGS.cf, sizeof(op1))
#define op2_cmp(op1, op2)       (void)_op2_sub(op1, op2, 1, sizeof(op1))
// This gives us the wrong AF, but no one cares.
#define op1_neg(op1)            op1 = _op2_sub(0, op1, 1, sizeof(op1))


static inline void op0_daa(void)
{
    unsigned old_al = al;
    unsigned old_cf = EFLAGS.cf;
    EFLAGS.cf = 0;
    if ((al & 0xf) > 9 || EFLAGS.af) {
        al = al + 6;
        EFLAGS.cf = old_cf || (al > 0xA);
        EFLAGS.af = 1;
    }
    else
        EFLAGS.af = 0;
    if (old_al > 0x99 || old_cf) {
        al = al + 0x60;
        EFLAGS.cf = 1;
    }
    else
        EFLAGS.cf = 0;
    EFLAGS.sf = (al & 0x80) != 0;
    EFLAGS.zf = al == 0;
}

static inline void op0_das(void)
{
    unsigned old_al = al;
    unsigned old_cf = EFLAGS.cf;
    EFLAGS.cf = 0;
    if ((al & 0xf) > 9 || EFLAGS.af) {
        al = al - 6;
        EFLAGS.cf = old_cf || (al < 6);
        EFLAGS.af = 1;
    }
    else
        EFLAGS.af = 0;
    if (old_al > 0x99 || old_cf) {
        al = al - 0x60;
        EFLAGS.cf = 1;
    }
    EFLAGS.sf = (al & 0x80) != 0;
    EFLAGS.zf = al == 0;
}


static inline uint32_t _op1_incdec(uint32_t op1, int dir, size_t w)
{
    uint64_t tmp = (uint64_t)op1 + dir;
    uint64_t cmask = 1ULL << (w * 8);
    uint64_t smask = 1ULL << (w * 8 - 1);
    EFLAGS.of = ((op1 & smask) == (dir & smask)) &&
        ((tmp & smask) != (op1 & smask));
    EFLAGS.sf = (tmp & smask) != 0;
    if (dir > 0)
        EFLAGS.af = (op1 & 0xf) + (dir & 0xf) > 0xf;
    else
        EFLAGS.af = (op1 & 0xf) + (dir & 0xf) <= 0xf;
    tmp = tmp & (cmask - 1);
    EFLAGS.zf = tmp == 0;
    return tmp;
}

#define op1_inc(op1)            op1 = _op1_incdec(op1, 1, sizeof(op1))
#define op1_dec(op1)            op1 = _op1_incdec(op1, -1, sizeof(op1))


static inline uint32_t uf_shift(uint32_t tmp, uint32_t op1, int lcnt, size_t w)
{
    if (lcnt) {
        uint64_t cmask = 1ULL << (w * 8);
        uint64_t smask = 1ULL << (w * 8 - 1);
        EFLAGS.of = (tmp & smask) != (op1 & smask);
        if (lcnt > 0)
            EFLAGS.cf = (op1 & (smask >> (lcnt - 1))) != 0;
        else
            EFLAGS.cf = (op1 & (1ULL << (-lcnt - 1))) != 0;
        EFLAGS.sf = (tmp & smask) != 0;
        tmp = tmp & (cmask - 1);
        EFLAGS.zf = tmp == 0;
    }
    return tmp;
}

static inline uint32_t _op2_shl(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t tmp = op1 << cnt;
    return uf_shift(tmp, op1, cnt, w);
}

static inline uint32_t _op2_shr(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t tmp = op1 >> cnt;
    return uf_shift(tmp, op1, -cnt, w);
}

static inline uint32_t _op2_sar(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    int32_t smask = 1ULL << (w * 8 - 1);
    int32_t tmp = op1;
    tmp |= ~((op1 & smask) - 1);                // sign-extend
    tmp = tmp >> cnt;
    return uf_shift(tmp, op1, -cnt, w);
}

static inline uint32_t _op2_rol(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t smask = 1ULL << (w * 8 - 1);
    uint32_t tmp = op1;
    unsigned cf = EFLAGS.cf;
    while (cnt--) {
        unsigned tempcf = (tmp & smask) != 0;
        tmp = (tmp << 1) | tempcf;
    }
    cnt = op2 & 0x1f;
    return uf_shift(tmp, op1, cnt, w);
}

static inline uint32_t _op2_ror(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t smask = 1ULL << (w * 8 - 1);
    uint32_t tmp = op1;
    unsigned cf = EFLAGS.cf;
    while (cnt--) {
        unsigned tempcf = (tmp & 1) != 0;
        tmp = (tmp >> 1) | (tempcf * smask);
    }
    cnt = op2 & 0x1f;
    return uf_shift(tmp, op1, -cnt, w);
}

static inline uint32_t _op2_rcl(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t smask = 1ULL << (w * 8 - 1);
    uint32_t tmp = op1;
    unsigned cf = EFLAGS.cf;
    while (cnt--) {
        unsigned tempcf = (tmp & smask) != 0;
        tmp = (tmp << 1) | cf;
        cf = tempcf;
    }
    cnt = op2 & 0x1f;
    return uf_shift(tmp, op1, cnt, w);
}

static inline uint32_t _op2_rcr(uint32_t op1, uint32_t op2, size_t w)
{
    int cnt = op2 & 0x1f;
    uint32_t smask = 1ULL << (w * 8 - 1);
    uint32_t tmp = op1;
    unsigned cf = EFLAGS.cf;
    while (cnt--) {
        unsigned tempcf = (tmp & 1) != 0;
        tmp = (tmp >> 1) | (cf * smask);
        cf = tempcf;
    }
    cnt = op2 & 0x1f;
    return uf_shift(tmp, op1, -cnt, w);
}

#define op2_shl(op1, op2)       op1 = _op2_shl(op1, op2, sizeof(op1))
#define op2_shr(op1, op2)       op1 = _op2_shr(op1, op2, sizeof(op1))
#define op2_sar(op1, op2)       op1 = _op2_sar(op1, op2, sizeof(op1))
#define op2_rol(op1, op2)       op1 = _op2_rol(op1, op2, sizeof(op1))
#define op2_ror(op1, op2)       op1 = _op2_ror(op1, op2, sizeof(op1))
#define op2_rcl(op1, op2)       op1 = _op2_rcl(op1, op2, sizeof(op1))
#define op2_rcr(op1, op2)       op1 = _op2_rcr(op1, op2, sizeof(op1))

static inline void op1_mul_byte(uint8_t op1)
{
    uint16_t tmp = al * (uint16_t)op1;
    al = (uint8_t)tmp;
    ah = (uint8_t)(tmp >> 8);
}

static inline void op1_mul_word(uint16_t op1)
{
    uint32_t tmp = ax * (uint32_t)op1;
    ax = (uint16_t)tmp;
    dx = (uint16_t)(tmp >> 16);
}

static inline void op1_mul_dword(uint32_t op1)
{
    uint64_t tmp = eax * (uint64_t)op1;
    eax = (uint32_t)tmp;
    edx = (uint32_t)(tmp >> 32);
}

static inline void op1_imul_byte(uint8_t op1)
{
    int16_t tmp = (int16_t)(int8_t)al * (int16_t)(int8_t)op1;
    al = (uint8_t)tmp;
    ah = (uint8_t)(tmp >> 8);
}

static inline void op1_imul_word(uint16_t op1)
{
    int32_t tmp = (int32_t)(int16_t)ax * (int32_t)(int16_t)op1;
    ax = (uint16_t)tmp;
    dx = (uint16_t)(tmp >> 16);
}

static inline void op1_imul_dword(uint32_t op1)
{
    int64_t tmp = (int64_t)(int32_t)eax * (int64_t)(int32_t)op1;
    eax = (uint32_t)tmp;
    edx = (uint32_t)(tmp >> 32);
}

static inline uint8_t _op2_imul_byte(uint8_t op1, uint8_t op2)
{
    int16_t tmp = (int16_t)(int8_t)op1 * (int16_t)(int8_t)op2;
    return (uint8_t)tmp;
}

static inline uint16_t _op2_imul_word(uint16_t op1, uint16_t op2)
{
    int32_t tmp = (int32_t)(int16_t)op1 * (int32_t)(int16_t)op2;
    return (uint16_t)tmp;
}

static inline uint32_t _op2_imul_dword(uint32_t op1, uint32_t op2)
{
    int64_t tmp = (int64_t)(int32_t)op1 * (int64_t)(int32_t)op2;
    return (uint32_t)tmp;
}

#define op2_imul_byte(op1, op2)         op1 = _op2_imul_byte(op1, op2);
#define op2_imul_word(op1, op2)         op1 = _op2_imul_word(op1, op2);
#define op2_imul_dword(op1, op2)        op1 = _op2_imul_dword(op1, op2);


static inline void op1_div_word(uint16_t op1)
{
    uint16_t quot = ax / op1;
    uint16_t rem = ax - quot * op1;
    ax = quot;
    dx = rem;
}

static inline void op1_div_dword(uint32_t op1)
{
    uint32_t quot = eax / op1;
    uint32_t rem = eax - quot * op1;
    eax = quot;
    edx = rem;
}


#define op0_clc()               EFLAGS.cf = 0
#define op0_stc()               EFLAGS.cf = 1
#define op0_cmc()               EFLAGS.cf = !EFLAGS.cf;

#define op0_lahf()              ah = (uint8_t)_st._flg.r
#define op0_sahf()              _st._flg.r = ah | (_st._flg.r & ~0xFF)


#define _branch(dst, cond)      if (cond) goto dst

#define op1_ja(op1)             _branch(op1, !(EFLAGS.cf | EFLAGS.zf))
#define op1_jae(op1)            _branch(op1, !EFLAGS.cf)
#define op1_jb(op1)             _branch(op1, EFLAGS.cf)
#define op1_jbe(op1)            _branch(op1, EFLAGS.cf | EFLAGS.zf)
#define op1_jc(op1)             _branch(op1, EFLAGS.cf)
#define op1_je(op1)             _branch(op1, EFLAGS.zf)
#define op1_jmp(op1)            _branch(op1, 1)
#define op1_jne(op1)            _branch(op1, !EFLAGS.zf)
#define op1_jng(op1)            _branch(op1, (EFLAGS.sf ^ EFLAGS.of) | EFLAGS.zf)
#define op1_jnl(op1)            _branch(op1, !(EFLAGS.sf ^ EFLAGS.of))
#define op1_jo(op1)             _branch(op1, EFLAGS.of)
#define op1_jno(op1)            _branch(op1, !EFLAGS.of)
#define op1_js(op1)             _branch(op1, EFLAGS.sf)
#define op1_jz(op1)             op1_je(op1)
#define op1_jnz(op1)            op1_jne(op1)

#define op1_call(op1)           op1()
#define op0_ret()               return


static inline void _op1_push(uint32_t op1, size_t w)
{
    esp -= sizeof(w);
    if (w == 4)
        m_dword(esp) = op1;
    else if (w == 2)
        m_word(esp) = (uint16_t)op1;
    else
        m_byte(esp) = (uint8_t)op1;
}

static inline uint32_t _op1_pop(size_t w)
{
    uint32_t ret;
    if (w == 4)
        ret = m_dword(esp);
    else if (w == 2)
        ret = m_word(esp);
    else
        ret = m_byte(esp);
    esp += sizeof(w);
    return ret;
}

#define op1_push(op1)           _op1_push(op1, sizeof(op1))
#define op1_pop(op1)            op1 = _op1_pop(sizeof(op1))
