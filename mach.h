/*
 * Copyright (C) 2023 David Hunter
 *
 * This file is part of the zsnes-asm2c project.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#pragma once

#include <stdint.h>
#include <stdio.h>

typedef struct {
    unsigned voice_disables;
} mach_init_t;

void mach_init(const mach_init_t* init);
void mach_init_state(void);
void mach_init_mem(void);
void mach_dump_mem(const char* path);
int mach_fwrite_membuf(FILE* f, unsigned addr, size_t len);
