# zsnes-asm2c: C-language translation of the ZSNES audio engine

## Introduction

zsnes-asm2c is a C-language translation of the audio engine (SPC700 + S-DSP) from the [ZSNES](https://www.zsnes.com/) 0.15 emulator. (Version 0.15 was the first beta release.)

Currently, the project builds as a utility program to load .spc files and output raw audio (16-bit, mono, 44.1kHz). The end goal is to transplant this engine into another emulator (eg Snex9x).


## History

At the time I started this project, the source code to ZSNES 0.15 was not available, and the oldest published source was for version 1.17b. To reconstruct source (100% x86 assembly) for ZSNES 0.15, I disassembled the ZSNES 0.15 binary, then edited the 1.17b source until its disassembled output (built using NASM and i686-elf binutils) matched the 0.15 disassembly. That restored variable names and labels, critical aids to understanding the program's operation.

Given the reconstructed ZSNES 0.15 source, I then:

1. created a trivial x86 assembly to C converter -- converts instructions to C macros
2. wrote a simulated x86 host (RAM and 80386 [real mode] CPU state) and implemented the required instructions
3. loaded an .spc file into the host
4. called the SPC700 execution engine, timer update, and DSP sample generation code at the right frequency
5. debugged until music emerged


## Build

macOS:

- Install Xcode Command Line Tools
- Run `make`

Windows:

- I'm told Windows Subsystem for Linux (WSL) is the way to go here.
- Run `make`

Linux:

- Install GNU Make, a C compiler (eg gcc) and Python 3.x
- Run `make`

### tests

Files in tests/ are for me to develop the x86 instruction macros and validate them. You don't need to run them. But if you do, install the [GNU check library](https://libcheck.github.io/check/) and run `make check` from the top level directory.


## Run

```
./spcplay /path/to/your-song.spc
```

Take the output (by default, `play.raw`) and play or convert to your audio format of choice.

Example for SOX:

```
play -b 16 -r 44100 -c 1 -L -e signed-integer play.raw
```


## Links

ZSNES: <https://www.zsnes.com/>

Oldest published ZSNES source, version 1.17b: <https://sourceforge.net/projects/zsnes/files/zsnes/ZSNES%20v1.17%20source/zsnes117b-src.zip/download>

.spc file: [vgmpf.com : SPC](http://www.vgmpf.com/Wiki/index.php?title=SPC#:~:text=%3F,-First%20Game%3A&text=The%20SPC%20format%20holds%20SNES,SNES%27s%20S%2DSMP%20audio%20chip.)

The (SNES) music archive: <http://snesmusic.org>


## Copying

zsnes-asm2c is free software. See COPYING for copying permission.

ZSNES sources (dsp.asm, spc700.asm) were copied from ZSNES version 1.17b and modified as described above in the History section.
